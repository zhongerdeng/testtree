const Service = require('egg').Service;
class BaseService extends Service {
    async get(url){
        const result = await this.app.curl(url, {
            dataType: 'json',
        });
        return result.data;;
    }    
}

module.exports = BaseService;