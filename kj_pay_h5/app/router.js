module.exports = app=>{

    const { router, controller } = app;

    router.get('/auth', controller.auth.index);

    router.get('/*', controller.home.index);

    router.post('/qiniu', controller.qiniu.qiniuUploadStream);
    router.post('/api', controller.api.index);
 
}