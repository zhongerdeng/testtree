module.exports = (app) => {
  
    class HomeController extends app.Controller {
      async index() {
        // render view template in `app/views`

        const {ctx,app} = this;
        let q = ctx.query;

        let originalUrl =  ctx.request.originalUrl;

       // console.log(originalUrl);
        
        if (q.openid == undefined){
          
          let url = ctx.request.originalUrl;

        await ctx.redirect(`http://kjh5.atogether.com/auth?redirect=${url}`)
         // await ctx.redirect(`http://192.168.3.105:9001?auth?redirect=${url}`)
        }else{

            let isBand = true;
            let openid = q.openid;
            let userid = 0;
            //判断openid是否绑定

            let params = {
                openId:openid
            }
            let header = {};
            const result = await ctx.curl("http://192.168.3.105:9001/user/getUserByOpenId?_xyz", {
                // 自动解析 JSON response
                dataType: 'json',
                method: 'POST',
                data:params,
                headers: header,
                // 3 秒超时
                timeout: 3000,
              });
            //let rs = {}
            let rs = result.data;
            if(rs.success){

                userid = rs.data
                console.log('userid:'+userid)
            }else {}

          
            if (!isBand){
              if (originalUrl.indexOf("register") >= 0){
                await this.ctx.render('index',{
                  openid:openid,
                  userid:userid
                });
              }else{
                await ctx.redirect(`/register?openid=${openid}`)
              }
            }else{

              await this.ctx.render('index',{
                openid:openid,
                userid:userid,
              });
            }
        }

      }
    }
  
    return HomeController;
  };