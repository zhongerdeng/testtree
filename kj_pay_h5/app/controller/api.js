'use strict';

const Controller = require('../core/base_controller');
var WechatAPI = require('co-wechat-api');

class ApiController extends Controller {
  async index() {
    const {ctx,app} = this;
    let data = ctx.request.body;
    let params = data.params;

    let token = params.token;
    let header = {};

    // if (token != undefined){
    //    header = {"Authorization":"Bearer "+token,}
    // }
    

    const result = await ctx.curl(data.url, {
      // 自动解析 JSON response
      dataType: 'json',
      method: 'POST',
      data:params,
      headers: header,
      // 3 秒超时
      timeout: 3000,
    });

    let rs = result.data;

    if (rs.success){
        this.success(rs.data);
    }else{
        this.error(rs.message);
    }

  }

  async getJsConfig(){
      const {ctx,app} = this;
      var url =  ctx.request.get('Referer');
      let data = ctx.request.body;
      const api = new WechatAPI('wx39645b146d3d2f41', '5fd27155e9372656ef3f2d4a03fb2a54');
      
      let apilist = data.apilist;

      let jsApiList = apilist.split(';')

      var param = {
          debug: false,
          jsApiList:jsApiList,
          url: url
      };
      let result = await api.getJsConfig(param);
      this.success(result) ;
  }
}

module.exports = ApiController;
