'use strict';

const Controller = require('../core/base_controller');
var OAuth = require('co-wechat-oauth');
var client = new OAuth('wx941a76c096d31139', '5ea387781df522350c7e5bfbbbc7331a');

class AuthController extends Controller {
  async index() {
    const {ctx,app} = this;
    let query = ctx.query;
    let redirect = query.redirect;

    redirect =  encodeURIComponent(redirect)

    console.log("请求Code");

    let code = query.code;

    if (code == undefined){
        const url = client.getAuthorizeURL('http://kjh5.atogether.com/auth?redirect='+redirect, '', 'snsapi_userinfo');
        await ctx.redirect(url);
    }else{
        try {
            var token = await client.getAccessToken(code);
            var accessToken = token.data.access_token;
            var openid = token.data.openid;

            let newredirect =  decodeURIComponent(redirect);
            let frontUrlArray = newredirect.split("?");
            let newUrl = "";
            newUrl = `${frontUrlArray[0]}?openid=${openid}`;
            if (frontUrlArray.length > 1){
                newUrl = `${frontUrlArray[0]}?${frontUrlArray[1]}&openid=${openid}`;
            }

            await ctx.redirect(newUrl);
        } catch (error) {
            const url = client.getAuthorizeURL('http://kjh5.atogether.com/auth', '', 'snsapi_userinfo');
            await ctx.redirect(url);
        }
    }
  }
}

module.exports = AuthController;
