import React, { Component } from 'react';
import News from 'client/news'
import Pay from 'client/page/pay'
import Register from 'client/page/register'
import Login from 'client/page/login'
import Renzheng from 'client/page/renzheng'
import Kaihu from 'client/page/kaihu'
import Shenhe from 'client/page/waitshenhe'
import Shoukuan from 'client/page/shoukuan'
import Shoukuaninfo from 'client/page/shoukuaninfo'
import EncourageMoney from 'client/page/EncourageMoney'
import OwnInfo from 'client/page/ownInfo'
import MyOrder from 'client/page/myOrder'
import Promote from 'client/page/promote'
import Upgrade from 'client/page/upgrade'
import ResultAction from 'client/page/resultAction'
import WaitUpGrade from 'client/page/waitUpGrade'
import Help from 'client/page/help'
import PromptInfo from 'client/page/PromptInfo'
import Failed from 'client/page/failed'
import Testant from 'client/page/test'

import { BrowserRouter, StaticRouter, Route, Link } from 'react-router-dom';
const Router = __CLIENT__ ? BrowserRouter : StaticRouter;

import { UserContext } from './userContext';

const App = props => {
    return(
       <UserContext.Provider value={{ userInfo: props.userInfo}}>
            <Router {...props}>
                    <div style={{height:'100%'}}>
                        <Route path='/news' component={News}/>
                        <Route path="/pay" component={Pay} />
                        <Route path="/register" component={Register}/>
                        <Route path="/login" component={Login} />
                        <Route path="/renzheng" component={Renzheng} />
                        <Route path="/kaihu" component={Kaihu} />
                        <Route path="/waitshenhe" component={Shenhe} />
                        <Route path="/shoukuan" component={Shoukuan} />
                        <Route path="/shoukuaninfo" component={Shoukuaninfo} />
                        <Route path="/OwnInfo" component={OwnInfo} />
                        <Route path="/EncourageMoney" component={EncourageMoney} />
                        <Route path="/myOrder" component={MyOrder} />
                        <Route path="/promote" component={Promote} />
                        <Route path="/upgrade" component={Upgrade} />
                        <Route path="/resultAction" component={ResultAction} />
                        <Route path="/waitUpGrade" component={WaitUpGrade} />
                        <Route path="/help" component={Help} />
                        <Route path="/PromptInfo" component={PromptInfo} />
                        <Route path="/failed" component={Failed} />
                        <Route path="/test" component={Testant} />
                    </div>
            </Router>
        </UserContext.Provider>
    )
}

export default App;


