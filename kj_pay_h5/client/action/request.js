import fetchUtil from '../utils/fetchUtil'

//const apiPrex = "http://192.168.3.105:9001";
const apiPrex = "http://kjapi.atogether.com";

const queryData = async (url,params) => {

    return new Promise(function(resolve,reject){
        fetchUtil.commonRequest(apiPrex+url,params).then((data) => {
            resolve(data);
        })
    }).then((data)=>{
        return data
    })
}

export default queryData;