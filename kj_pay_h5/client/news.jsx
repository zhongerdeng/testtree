import React from 'react';

import {WhiteSpace,List,Button,NoticeBar} from 'antd-mobile';

export default class News extends React.Component {
 render() {
   return(
    <div>
    <h2>Beidou advanced example</h2>
    <a href="/about">about</a>
    <WhiteSpace />
            <Button type='primary' disabled>按钮</Button>
            <NoticeBar marqueeProps={{ loop: true }} style={{width:'400px'}}>
      Notice: The arrival time of incomes and transfers of Yu &#39;E Bao will be delayed during National Day.
    </NoticeBar>
    <WhiteSpace size="lg" />
    <NoticeBar mode="link" onClick={() => alert('1')} style={{width:'400px'}}>
      Notice: The arrival time of incomes and transfers of Yu &#39;E Bao will be delayed during National Day.
    </NoticeBar>
  </div>
   );
 }
}