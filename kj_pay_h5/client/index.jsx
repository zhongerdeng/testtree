import React from 'react';
import ReactDOM from 'react-dom';
import App from './app';

export default class View extends React.Component {

  static getPartial(props) {
    const { ctx,openid,userid } = props;
    console.log(props)
    return {
      html: <App userInfo={{
        userid:userid,
        openid:openid
      }} location={ctx.req.url} context={{}} />,
    };
  }
  render() {
    const { html,helper,userid,openid } = this.props;
    const globalStyle = {
      height:'100%',
    }

    let userInfo={
      userid:userid,
      openid:openid
    };

    return (
      <html style={globalStyle}>
      <head>
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, minimum-scale=1, user-scalable=no" />
        <script src="https://as.alipayobjects.com/g/component/fastclick/1.0.6/fastclick.js"></script>
        <link href={helper.asset('index.css')} rel="stylesheet" type="text/css" />
        <link href={helper.asset('manifest.css')} rel="stylesheet" type="text/css" />
        </head>
        <body style={globalStyle}>
        <div id="container" dangerouslySetInnerHTML={{ __html: html }} style={globalStyle}/>
        <script dangerouslySetInnerHTML={{ __html: `window.$$data=${JSON.stringify(userInfo)}` }} />
        <script src={helper.asset('manifest.js')} />
        <script src={helper.asset('index.js')} />
        <script src="http://res.wx.qq.com/open/js/jweixin-1.2.0.js"></script>
        </body>
      </html>
    );
  }
}

if (__CLIENT__) {
    const userInfo = window.$$data;
    ReactDOM.hydrate(<App userInfo={userInfo} />, document.getElementById('container'));
  }