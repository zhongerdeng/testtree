import React, { Component } from 'react';
import './mypay.css'
import queryData from '../action/request'
import DocumentTitle from 'react-document-title'
import { UserContext } from '../userContext';
import PropTypes from 'prop-types'

var indexImg = 1;
var index = 0;
const levelImg = [
    '',
    'http://pfnew9fvk.bkt.clouddn.com/%E6%99%AE%E9%80%9A%E6%97%A0%E8%83%8C%E6%99%AF.png',
    'http://pfnew9fvk.bkt.clouddn.com/%E9%BB%84%E9%87%91%E6%97%A0%E8%83%8C%E6%99%AF.png',
    'http://pfnew9fvk.bkt.clouddn.com/%E9%92%BB%E7%9F%B3%E6%97%A0%E8%83%8C%E6%99%AF.png',
    '',
]
const levelMoney = [
    0,199,399
]
const levelUser = [
    0,3,10
]
class Upgrade extends Component{

    constructor(props) {
        super(props); 
        this.state = {
            id:props.userInfo.userid,
            levelTxt : [
                '普通合伙人','黄金合伙人','钻石合伙人'
            ],
            lastImg:levelImg[0],
            curImg:levelImg[1],
            nextImg:levelImg[2],
            levelMoney:levelMoney[0],
            levelUser:levelUser[0],
            btnDisplay:'hidden'
        }
    }   
    clickBack  = () => {

        if(indexImg > 1)indexImg --;
        if(index > 0)index --;
        if(index > 0){
            this.setState({btnDisplay:'visible'})
        }else if(index == 0)this.setState({btnDisplay:'hidden'})

        this.setState({
            lastImg:levelImg[indexImg-1],
            curImg:levelImg[indexImg],
            nextImg:levelImg[indexImg+1],
            levelMoney:levelMoney[index],
            levelUser:levelUser[index]
        })
        console.log(indexImg)
    }
    clickForward  = () => {

        if(indexImg < 3)indexImg ++;
        if(index < 2)index ++;
        if(index > 0){
            this.setState({btnDisplay:'visible'})
        }else if(index == 0)this.setState({btnDisplay:'hidden'})

        this.setState({
            lastImg:levelImg[indexImg-1],
            curImg:levelImg[indexImg],
            nextImg:levelImg[indexImg+1],
            levelMoney:levelMoney[index],
            levelUser:levelUser[index]
        })
        console.log(indexImg)
    }
    clickUpgrade = () => {
        this.context.router.history.push('./waitUpGrade')
    }
    render(){
    return (
        <DocumentTitle title="升级中心">
        <div style={body}>
            <div style={divpart_1}>
                <div style={{width:'12%',height:'80%',float:'left'}}>
                    <div className="h_center v_center" style={back} onClick={this.clickBack}/>
                </div>
                <div style={{width:'20%',height:'80%',float:'left'}}>
                    <div className="h_center v_center" style={{
                        width:'90%',
                        height:'70px',
                        backgroundImage:`url(${this.state.lastImg})`,
                        backgroundSize:'100% 100%',
                        backgroundPosition:'50% 50%',
                        backgroundRepeat:'no-repeat',
                    }}/>
                </div>
                <div style={{width:'36%',height:'80%',float:'left'}}>
                    <div style={{width:'100%',height:'70%',float:'left'}}>
                        <div className="h_center v_center" style={{
                            width:'100px',
                            height:'110px',
                            backgroundImage:`url(${this.state.curImg})`,
                            backgroundSize:'100% 100%',
                            backgroundPosition:'50% 50%',
                            backgroundRepeat:'no-repeat',
                        }}/>
                    </div>
                    <div style={{width:'100%',height:'30%',float:'left'}}>
                        <div style={{width:'100%',height:'auto',float:'left',textAlign:'center'}}>
                            <span style={{color:'white',fontSize:'1.1em'}}>{this.state.levelTxt[index]}</span>
                        </div>
                    </div> 
                </div>
                <div style={{width:'20%',height:'80%',float:'left'}}>
                    <div className="h_center v_center" style={{
                        width:'90%',
                        height:'70px',
                        backgroundImage:`url(${this.state.nextImg})`,
                        backgroundSize:'100% 100%',
                        backgroundPosition:'50% 50%',
                        backgroundRepeat:'no-repeat',
                    }}/>
                </div>
                <div style={{width:'12%',height:'80%',float:'left'}}>
                    <div className="h_center v_center" style={forward} onClick={this.clickForward}/>
                </div>
            </div>
            <div style={divpart_2}>
                <div className = "h_center" style={divpart_3}>
                    <div style={divpart_4}>
                        <div style={{width:'50%',height:'100%',float:'left'}}>
                            <div style={{width:'50%',height:'100%',float:'left'}}>
                                <div className="h_center v_center" style={userIcon}/>
                            </div>
                            <div style={{width:'50%',height:'100%',float:'left'}}>
                                <span className="h_center v_center">
                                    <font style={{color:'#9b9b9b'}}>普通</font><br/>
                                    <font style={{color:'#9b9b9b',fontSize:'0.7em'}}>当前等级LV.1</font>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div style={divpart_5}>
                        <div style={{width:'100%',height:'40%',float:'left'}}>
                            <div style={{width:'100%',height:'32%',float:'left',textAlign:'center'}}>
                                <span className="v_center" style={{color:'#2e76ff'}}>级别权益</span>
                            </div>
                            <div style={{width:'100%',height:'68%',float:'left'}}>
                                <div style={divbox_1}>
                                    <div style={divspan_1}><span>收款方式</span></div>
                                    {/* <div style={divspan_1}><span>POS收款</span></div> */}
                                    <div style={divspan_1}><span>快捷收款</span></div>
                                </div>
                                <div style={divbox_1}>
                                    <div style={divspan_1}><span>交易费率</span><br/></div>
                                    {/* <div style={divspan_1}><span>5.5‰ + 3</span><br/></div> */}
                                    <div style={divspan_1}><span>5.5‰ + 2</span><br/></div>
                                </div>
                                <div style={divbox_1}>
                                    <div style={divspan_1}><span>直接伙伴交易</span><br/></div>
                                    {/* <div style={divspan_1}><span>0.4‰分润</span><br/></div> */}
                                    <div style={divspan_1}><span>0.1‰分润</span><br/></div>
                                </div>
                            </div>
                        </div>
                        <div style={{width:'100%',height:'60%',float:'left',textAlign:'center'}}>
                            <div style={{width:'100%',height:'30%',float:'left'}}>
                                <span className="v_center" style={{color:'#2e76ff'}}>升级{this.state.levelTxt[index]}等级方式</span>
                            </div>
                            <div style={{width:'100%',height:'70%',float:'left'}}>
                                {/* <div style={divbox_2}>
                                    <div style={divbox_3}>
                                        <div className="h_center v_center"style={icon1}/>
                                    </div>
                                    <div style={divbox_3}>
                                        <span className=" v_center">参加官方活动<br/>获得免费升级名额</span>
                                    </div>
                                </div> */}
                                <div style={divbox_2}>
                                    <div style={divbox_3}>
                                        <div className="h_center v_center"style={icon2}/>
                                    </div>
                                    <div style={divbox_4}>
                                        <span className=" v_center">付费升级<br/><font style={{color:'#2e76ff'}}>{this.state.levelMoney}</font>元升级费用</span>
                                    </div>
                                </div>
                                <div style={divbox_2}>
                                    <div style={divbox_3}>
                                        <div className="h_center v_center"style={icon3}/>
                                    </div>
                                    <div style={divbox_4}>
                                        <span className="v_center">成功推荐<font style={{color:'#2e76ff'}}>{this.state.levelUser}</font>个用户<br/>激活后系统自动升级</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div style={{
                        width:'100%',
                        height:'20%',
                        float:'left',
                        background:'white',
                        visibility:`${this.state.btnDisplay}`
                    }}>
                        <div className="h_center v_center" style={divBtn} onClick={this.clickUpgrade}>
                            <span className="h_center v_center">用 户 升 级</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </DocumentTitle>
        )
    }
}
const UpgradeConsumer = ({ }) => (
    <UserContext.Consumer>
        {user => {
            return (
                <Upgrade
                    userInfo={user.userInfo}
                />
            )
        }}
    </UserContext.Consumer>
)
export default UpgradeConsumer
Upgrade.contextTypes = {
    router: PropTypes.object.isRequired
}
const body = {
    width:'100%',
    height:'100%',
    float:'left'
}
const divpart_1 = {
    width:'100%',
    height:'30%',
    float:'left',
    background:'#2e76ff'
}
const divpart_2 = {
    width:'100%',
    height:'70%',
    float:'left'
}
const divpart_3 = {
    width:'92%',
    height:'100%',
    marginTop:'-15%',
    opcity:'0',
}
const divpart_4 = {
    width:'100%',
    height:'24%',
    float:'left',
    backgroundColor:'#2e76ff',
    backgroundImage:'url(http://pfnew9fvk.bkt.clouddn.com/%E5%8D%87%E7%BA%A7%E5%85%A5%E5%8F%A3.png)',
    backgroundSize:'100% 100%',
    backgroundPosition:'50% 50%',
    backgroundRepeat:'no-repeat',
    borderBottomLeftRadius:'10px',
    borderBottomRightRadius:'10px'
}
const divpart_5 = {
    width:'100%',
    height:'55%',
    float:'left',
    marginTop:'1%',
    background:'white',
}
const divBtn = {
    width:'82%',
    height:'2.1em',
    color:'white',
    fontSize:'1em',
    background:'#2e76ff',
    textAlign:'center',
    borderRadius:'25px'
}
const divbox_1 = {
    width:'33.3%',
    height:'100%',
    float:'left',
    textAlign:'center',
    color:'#9b9b9b',
    fontSize:'0.9em'
}
const divbox_2 = {
    width:'50%',
    height:'100%',
    float:'left',
}
const divbox_3 = {
    width:'100%',
    height:'40%',
    float:'left',
    textAlign:'center',
    color:'#9b9b9b',
    fontSize:'0.9em',
}
const divbox_4 = {
    width:'100%',
    height:'60%',
    float:'left',
    textAlign:'center',
    color:'#9b9b9b',
    fontSize:'0.9em',
}
const divspan_1 = {
    width:'100%',
    height:'50%',
    float:'left',
}
const icon1 = {
    width:'40px',
    height:'40px',
    backgroundImage:'url(http://pfnew9fvk.bkt.clouddn.com/%E5%8D%87%E7%BA%A7%E5%85%A5%E5%8F%A3%E5%9B%BE%E6%A0%871.png)',
    backgroundSize:'100% 100%',
    backgroundPosition:'50% 50%',
    backgroundRepeat:'no-repeat',
    borderRadius:'50%',
}
let icon2 = {...icon1}
let icon3 = {...icon1}
let userIcon = {...icon1}
icon2.backgroundImage = 'url(http://pfnew9fvk.bkt.clouddn.com/%E5%8D%87%E7%BA%A7%E5%85%A5%E5%8F%A3%E5%9B%BE%E6%A0%872.png)'
icon3.backgroundImage = 'url(http://pfnew9fvk.bkt.clouddn.com/%E5%8D%87%E7%BA%A7%E5%85%A5%E5%8F%A3%E5%9B%BE%E6%A0%873.png)'
userIcon.width = '50px'
userIcon.height ='50px'
userIcon.backgroundImage = 'url(http://pfnew9fvk.bkt.clouddn.com/%E6%B5%8B%E8%AF%95%E5%A4%B4%E5%83%8F.jpg)'

const back = {
    width:'80%',
    height:'40px',
    backgroundImage:'url(http://pfnew9fvk.bkt.clouddn.com/back.jpg)',
    backgroundSize:'100% 100%',
    backgroundPosition:'50% 50%',
    backgroundRepeat:'no-repeat',
    borderRadius:'50%',
}
let forward = {...back}
forward.backgroundImage = 'url(http://pfnew9fvk.bkt.clouddn.com/forward.jpg)'