import React, { Component } from 'react';
import { Button, Icon ,List, InputItem, WhiteSpace,Checkbox} from 'antd-mobile';
// import { createForm } from 'rc-form';
import styles from './pay.css';
const CheckboxItem = Checkbox.CheckboxItem;

import classNames from 'classnames/bind';
const cx = classNames.bind(styles);

import fetchUtil from '../utils/fetchUtil'

const CheckButton = ({money,checked,onClick}) => {
    return <div 
    onClick={onClick}
    className={checked?styles.buttonPressed:styles.buttonNormal}>
        {money}元
    </div>
}

class App extends Component {

    constructor() {
        super();
        this.state = {
            equipId:0,
            equipNo:'',
            equipImg:'',
            inputValue:6,
            ischeck:false,
            payvalue:6,
            walletValue:0,
            equipStateString:'',
            equipState:0,//0：不可用，1：可用
        }
    }

    onChange = (value) => {
        console.log(value);
        this.setState({ischeck:!value})

        let walletValue = this.state.walletValue;
        let inputValue = this.state.inputValue;

        if (!value){
            if (walletValue > inputValue){
                this.setState({payvalue:0});
            }else{
                this.setState({payvalue:(parseFloat(inputValue)-parseFloat(walletValue)).toFixed(2)});
            }
        }else{
            this.setState({payvalue:inputValue});
        }
    }

    componentDidMount(){
        
    }

    pay = () => {

        let params = {
            userId:"019f9748606d461c84622ff7b799248b",
            tradeId:3,
            payType:1,
        }

        fetchUtil.commonRequest('http://lmm.hk1.tunnelfrp.cc/o_trade/pay',params).then((data) => {
        
            console.log(data);

            window.WeixinJSBridge.invoke(
                'getBrandWCPayRequest', {
                    "appId":data.appId,     //公众号名称，由商户传入     
                    "timeStamp":data.timeStamp,         //时间戳，自1970年以来的秒数     
                    "nonceStr":data.nonceStr, //随机串     
                    "package":data.package,     
                    "signType":'MD5',         //微信签名方式：     
                    "paySign":data.sign //微信签名 
                },
                function(res){    
                    if(res.err_msg == "get_brand_wcpay_request:ok" ) {
                    }     // 使用以上方式判断前端返回,微信团队郑重提示：res.err_msg将在用户支付成功后返回    ok，但并不保证它绝对可靠。 
                }
            ); 

        })

    }

    gotoPay = (id) => {
        
    }

    dealEquip = (id) => {
        
    }


    getEquipState = (equipNo) => {
        
    }

    gotoPaySuccess = () => {
    }

    onClickMoney = (value) => {
        this.setState({
            inputValue:value,
            ischeck:false,
            payvalue:value,
        })
    }

    render() {
      return (
        <div className={cx('container')}>
            
            <div className={cx('equipDiv')}>
                设备号：{this.state.equipNo}
            </div> 

            <Button onClick={() => {this.pay()}}  style={{marginBottom:'10px'}} type="primary">支付 1  元</Button>
        </div>
      );
    }
  }
  
  export default App;