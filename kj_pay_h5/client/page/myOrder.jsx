import React, { Component } from 'react';
import { Tabs, Badge, PullToRefresh , ListView, Toast, ActivityIndicator} from 'antd-mobile';
import './mypay.css'
import queryData from '../action/request'
import DocumentTitle from 'react-document-title'
import PromptInfo from './PromptInfo'
import { UserContext } from '../userContext';
import PropTypes from 'prop-types'

const tabs = [
    { title: <Badge>已结算</Badge> },
    { title: <Badge>未结算</Badge> },
  ];
class Myorder extends Component{

    constructor(props){
        super(props)
        const dataSource = new ListView.DataSource({
            rowHasChanged: (row1, row2) => row1 !== row2,
        });
        this.state={
            id:props.userInfo.userid,
            cardNumber:'6222***********4092',
            tabIndexStr:['已处理','未处理'],
            tabIndex:0,
            down: true,
            height:'100%',
          //  divHeight:'100%',
            dataSource,
            refreshing: true,
            isLoading: false,
            useBodyScroll: false,
            defaultTxt:['您还没有已结算的套现记录','您没有未结算的套现记录'],
            promptIndex:[4,5]
        }
        this.listData = [
            {dataArr:[],pIndex:1,count:0},
            {dataArr:[],pIndex:1,count:0},
        ]
    }
    componentDidUpdate() {
        if (this.state.useBodyScroll) {
          document.body.style.overflow = 'auto';
        } else {
          document.body.style.overflow = 'hidden';
        }
    }
    componentDidMount(){
        console.log(this.state.id)
        // const hei =  `${document.documentElement.clientHeight-ReactDOM.findDOMNode(this.refs.mytab).clientHeight}px`
        // this.setState({
        //     divHeight:hei,
        // })
        this.getOrderList(this.state.tabIndex);   
    }
    getOrderList = (status) => {
        const params = {
            id:this.state.id, 
            orderStatus : this.state.tabIndexStr[status],  
            page:this.listData[status].pIndex,  
            size:10
        }
        queryData('/order/getOrderList?_xyz',params).then((data)=>{

            let tmpArr = [];
            for (let i = 0; i < data.list.length; i++) {

            tmpArr.push(<div style={{width:'100%',height:'100%',float:"left"}}>
                        <div style={{width:'60%',height:'100%',float:"left"}}>
                            <span style={{fontSize:'1.1em'}}>&#8195;订单号{(this.listData[status].pIndex-1)*10+i}</span><br/>
                            <span style={{fontSize:'0.8em'}}>&#8195;{data.list[i].orderNo}</span><br/>
                            <span style={{fontSize:'0.8em'}}>&#8195;{data.list[i].createAt}</span>
                        </div>
                        <div style={{width:'40%',height:'100%',float:"left",textAlign:'right'}}>
                            <div className="v_center">
                                <span style={{fontSize:'1em'}}>套现&#8195;</span><br/>
                                <span style={{fontSize:'1em'}}>{data.list[i].orderAmount}元&#8195;</span>
                            </div> 
                        </div> 
                    </div>
                );
            }
            
            this.listData[status].dataArr = [...this.listData[status].dataArr ,...tmpArr]
            this.listData[status].count = data.count;
            this.setState({
                refreshing:false,
                isLoading:false,
                dataSource:this.state.dataSource.cloneWithRows(this.listData[status].dataArr)
            })
        })
        
    }  
    onEndReached = (event) => {

        if (this.state.isLoading && !this.state.hasMore) {
          return;
        }
        console.log('reach end', event);
        this.setState({ isLoading: true });
        if(this.listData[this.state.tabIndex].pIndex <= (this.listData[this.state.tabIndex].count/10+1)){
            ++this.listData[this.state.tabIndex].pIndex
            this.getOrderList(this.state.tabIndex)
        }       
    };
    onRefresh = () => {
        this.setState({ refreshing: true, isLoading: true });
        if(this.listData[this.state.tabIndex].pIndex <= (this.listData[this.state.tabIndex].count/10+1)){
            ++this.listData[this.state.tabIndex].pIndex
            this.getOrderList(this.state.tabIndex)
        }
    };

    showContent = (status) => {

        return this.listData[status].count > 0 ? 
        (<ListView
                ref={"myListView"}
                key={'0'}
                dataSource={this.state.dataSource}
                renderRow={(value)=>(
                    <div style={{width:'100%',height:'70px',float:'left',marginBottom:5,borderBottom:'1px solid #f1f1f1'}}>
                        {value}
                    </div>
                )}
                initialListSize={25}
                useBodyScroll={this.state.useBodyScroll}
                style={{height: this.state.height}}
                pullToRefresh={<PullToRefresh
                    refreshing={this.state.refreshing}
                    onRefresh={this.onRefresh}
                    down={false}
                    direction={'up'}
                    />}
                onEndReached={this.onEndReached}
                pageSize={5}
        />):
        (<PromptInfo title={'我的订单'} pmtInfoIndex={this.state.promptIndex[status]} history={this.context.router.history}/>) 
    };
    render(){
        
        return (
            <DocumentTitle title="我的订单">
            <div style={{width:'100%',height:'100%',background:'white'}} >
                <Tabs
                    ref={"mytab"} 
                    tabs={tabs}
                    initialPage={0}
                    onChange={(tab, index) => {
                            
                        this.listData[index].dataArr = [];
                        this.listData[index].pIndex = 1;
                        this.listData[index].count = 0;
                        this.setState({tabIndex:index})
                        this.getOrderList(index)
                    }}
                    onTabClick={(tab,index) => {
                
                        this.listData[index].dataArr = [];
                        this.listData[index].pIndex = 1;
                        this.listData[index].count = 0;
                        this.setState({tabIndex:index})
                        this.getOrderList(index)
                    }}>
                    {this.showContent(this.state.tabIndex)}
                </Tabs>
                {/* <div style={{
                    width:'100%',
                    height:`${this.state.divHeight}`,
                    float:'left',
                    background:'white'
                }}>
                    {this.showContent(this.state.tabIndex)}
                </div> */}
            </div>
            </DocumentTitle>
        )
    }
}
const MyorderConsumer = ({ }) => (
    <UserContext.Consumer>
        {user => {
            return (
                <Myorder
                    userInfo={user.userInfo}
                />
            )
        }}
    </UserContext.Consumer>
)
export default MyorderConsumer;
Myorder.contextTypes = {
    router: PropTypes.object.isRequired
}