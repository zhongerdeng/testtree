import React, { Component } from 'react';
import {Link} from "react-router-dom";
import { Toast} from 'antd-mobile';
import $ from 'jquery';
import './mypay.css'
import queryData from '../action/request'
import DocumentTitle from 'react-document-title'
import { UserContext } from '../userContext';

var num = 0;
var clock = null;
var phoneState = false;
var pwdState = false;
var verifyState = false; 
class Register extends Component{

    constructor(props) {
        super(props); 
        this.state = {
            bodyHeight:document.documentElement.clientHeight
        }
    }   
    componentDidMount(){

        console.log(this.props.userInfo.userid)

        const bodyHei = `${document.documentElement.clientHeight}px`
        this.setState({
            bodyHeight:bodyHei
        })
    }
    checkPhone = () => {

        var phone = $('[cgy=手机号]').val();
        if(phone.length == 0){
            $('[cgy=密码]').blur();
           
           Toast.fail('对不起，手机号不能为空', 1.5);
        }else if(phone.length == 11){
            $.ajax({
                //手机号查询
                url:'http://apis.juhe.cn/mobile/get?phone=' + phone + '&key=2237f7647c1b436ee6139dce0195ccbb',
                type: "GET",
                dataType: "jsonp", 
                success: function (res) {
                   // var result = JSON.stringify(res); //json对象转成字符串
                   // alert(result)
                     if(res.resultcode != '200'){
                        $('[cgy=密码]').blur();
                        
                        Toast.fail('请输入有效的手机号', 1.5);
                     }else{
                        phoneState = true;
                     }
                 }
             }); 
        }else {
            $('[cgy=密码]').blur();
           
           Toast.fail('请输入有效的手机号', 1.5);
        } 
    }
    clickgetVerify = () => {

        var pwd = $('[cgy=密码]').val();
        var phone = $('[cgy=手机号]').val();
        num = 60;
        if(pwd.length != 0){

            if(pwd.length >= 6 && pwd.length <=18){

                var res = /^[a-zA-Z]\w{6,18}$/;
                if(res.test(pwd)){

                    pwdState = true;

                    $('[cgy=手机号]').css("pointer-events","none")//禁用div事件
                    $('[cgy=密码]').css("pointer-events","none")
                    $('[cgy=获取验证码]').css("pointer-events","none")
                    
                    const params = {
                        phone:phone
                    }
                    queryData('/user/sendSms?_xyz',params,(data)=>{
                        console.log(data)
                    })

                    clock = window.setInterval(function(){  
                    $('[cgy=获取验证码]').find('span').text('重新获取(' + num  + ')')

                        if(num > 0){
                            num--;
                            console.log(num)
                        }
                        else {
                            //num = 15;
                            console.log(clock)
                            window.clearInterval(clock);
                            $('[cgy=获取验证码]').find('span').text('获取验证码')
                            $('[cgy=获取验证码]').css("pointer-events","auto")//启用div事件
                        }
                    },1000)
                }else{
                    Toast.fail('请输入正确格式的密码', 1.5);
                }
                
            }else if(pwd.length > 0 && pwd.length < 6 ){
               
               Toast.fail('密码必须六位以上', 1.5);
            }else if(pwd.length > 18){
               
                Toast.fail('密码不能超过十八位', 1.5);
            }
        }else {
            $('[cgy=验证码]').blur();
           
           Toast.fail('密码不能为空', 1.5);
        }
    }
    clickRegister = () => {
        
        var phone = $('[cgy=手机号]').val();
        var pwd = $('[cgy=密码]').val();
        var verify_code = $('[cgy=验证码]').val();
        
        window.clearInterval(clock);
        
        if(num > 0 && verify_code.length != 0){

            if(num > 0){
                verifyState = true;
            }else{
                verifyState = false;
                Toast.fail('对不起，无效的验证码', 1.5);
            }
        }
        if(phoneState == true && pwdState == true && verifyState == true){//成功
            console.log('成功验证')
            const params = {
                phone:phone,
                password:pwd,
                getVerifyCode:verify_code
            }
            queryData('/user/register?_xyz',params,(data)=>{
                console.log(data)
            })

            Toast.success('注册成功', 2,()=>{
                this.props.history.push('./renzheng')
               
            });

        }else{
           
            Toast.fail('对不起，请填写完整',1.5)
        } 
    }
    render(){
        
    return (
        <DocumentTitle title="用户注册">
        <div style={{
            width:'100%',
            height:`${this.state.bodyHeight}`,
            float:'left',
            background:'white'
        }}>
            <div style={divpart_1}/>
            <div style={divpart_2}>
            <div className="h_center" style={divCntBox}>
                    <div style={divRow}>
                        <div style={divimgBox}>
                            <div className="v_center" style={icon1}/>
                        </div>
                        <div className="v_center" style={divInputBox}>
                            <input style={divInput} cgy = "手机号" placeholder="请输入手机号码"/>
                        </div>
                    </div>
                    <div style={divRow}>
                        <div style={divimgBox}>
                            <div className="v_center" style={icon2}/>
                        </div>
                        <div className="v_center" style={divInputBox}>
                            <input cgy = "密码" type="password" placeholder="请输入登录密码" style={divInput} onFocus={()=>{this.checkPhone()}}/>
                        </div>
                        {/* <div style={{float:'left',fontSize:'0.5em'}}><span>密码输入强度</span></div> */}
                    </div>
                    <div style={divRow}>
                        <div style={divimgBox}>
                            <div className="v_center" style={icon3}/>
                        </div>
                        <div className="v_center" style={divInputBox}>
                            <div style={{width:'100%',height:'100%',float:'left'}}>
                                <input style={divInput} cgy = "验证码" placeholder="请输入验证码" />
                            </div>
                            <div cgy="获取验证码" className="v_center"  style={getVerify} onClick={()=>{this.clickgetVerify()}}>
                                <span className="v_center" >获取验证码</span>
                            </div> 
                        </div>
                    </div>
                    <div style={divtxt}>
                        <Link to={'/login'}>
                            <div style={{width:'auto',height:'24px',float:'right'}}>
                               
                                <div className="v_center" style={{width:'auto',height:'100%',float:'left',fontSize:'1.03em',color:'#3366ff',fontWeight:'600'}}>
                                    <span style={{color:"#848484"}}>已有帐号，登录&#8194;</span>
                                </div>
                            
                                <div className="v_center" style={{
                                    width:'11px',
                                    height:'13px',
                                    float:'left',
                                    backgroundImage:'url(http://pfnew9fvk.bkt.clouddn.com/%E7%99%BB%E5%BD%95%E5%8F%B3.png)',
                                    backgroundSize:'100% 100%',
                                    backgroundPosition:'0% 0%',
                                    backgroundRepeat:'no-repeat'
                                }}/>
                            </div>
                               
                         </Link>
                    </div>
                    <div style={divRow}>
                        <div cgy="注册" className="h_center v_center" style={divBtn} onClick={()=>{this.clickRegister()}}>
                            <span class="v_center">注&#8195;册</span>
                        </div>
                    </div>  
                </div>
            </div>
        </div>
        </DocumentTitle>
        )
    }
}

const RegisterConsumer = ({}) => (
    <UserContext.Consumer>
        {user => {
            return (
                <Register
                    userInfo={user.userInfo}
                />
            )
        }}
    </UserContext.Consumer>
)

export default RegisterConsumer

const icon1 = {
    width:'20px',
    height:'27px',
    float:'none',
    margin:'auto',
    backgroundImage:'url(http://pfnew9fvk.bkt.clouddn.com/%E6%89%8B%E6%9C%BA%E5%8F%B7%E7%A0%81.png)',
    backgroundSize:'100% 100%',
    backgroundRepeat:'no-repeat',
    backgroundPosition:'50% 50%',
}
let icon2 = {...icon1}
let icon3 = {...icon1}
icon2.width = '23px';
icon2.height = '26px';
icon2.backgroundImage = 'url(http://pfnew9fvk.bkt.clouddn.com/%E5%AF%86%E7%A0%81.png)';
icon3.width = '22px';
icon3.height = '20px';
icon3.backgroundImage = 'url(http://pfnew9fvk.bkt.clouddn.com/%E9%AA%8C%E8%AF%81%E7%A0%81.png)';


const divRow = {
    width:'100%',
    height:'auto',
    float:'left',
    marginTop:'35px'
}
const divimgBox = {
    width:'16%',
    height:'100%',
    float:'left'
}
const divInputBox = {
    width:'75%',
    height:'2.5em',
    float:'left',
    borderBottom:'1px solid #eeeeee',
}
const divInput = {
    width:'100%',
    height:'100%',
    float:'left',
    border:'0px',
    padding:'0px',
}
const divpart_1 = {
    width:'100%',
    height:'30%',
    float:'left',
    background:'url(http://pfnew9fvk.bkt.clouddn.com/%E6%B3%A8%E5%86%8C%E9%A1%B5.png)',
    backgroundSize:'100% 100%',
    backgroundRepeat:'no-repeat',
    backgroundPosition:'50% 50%' 
}
const divpart_2={
    width:'100%',
    height:'70%',
    float:"left",
}
const divCntBox = {
    width:'90%',
    height:'100%',
    float:'none',
    marginTop:'10px'
}
const getVerify = {
    width:'auto',
    height:'88%',
    color:'#2e76ff',
    right:'0px',
    position:'absolute',
   // background:'#2e76ff',
    border:'1px solid #2e76ff',
    borderRadius:'6px',
    paddingLeft:'6px',
    paddingRight:'6px',
    fontSize:'1.1em',

   // marginRight:'4px',
}
const divtxt = {
    width:'90%',
    height:'auto',
    float:'left',
    marginBottom:'20px'
}
const divBtn = {
    width:'92%',
    height:'2.3em',
    color:'white',
    fontSize:'1.15em',
    background:'#2e76ff',
    textAlign:'center',
    borderRadius:'25px'
}