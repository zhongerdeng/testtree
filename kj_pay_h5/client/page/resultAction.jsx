import React, { Component } from 'react';
import { Result, Icon, WhiteSpace } from 'antd-mobile';
import './mypay.css'
import DocumentTitle from 'react-document-title'
const myImg = src => <img src={src} className="spe am-icon am-icon-md" alt="" />;
class ResultAction extends Component{
    constructor(){
        super();
        this.state = {
            money:'9999',
            status:0,
            imgBox:[
                'http://pfnew9fvk.bkt.clouddn.com/%E7%AD%89%E5%BE%85%E5%A4%84%E7%90%86%E6%97%A0%E5%AD%97%E4%BD%93.png',
                'http://pfnew9fvk.bkt.clouddn.com/%E6%88%90%E5%8A%9F%E9%80%9A%E8%BF%87%E6%97%A0%E5%AD%97%E4%BD%93.png',
                'http://pfnew9fvk.bkt.clouddn.com/%E6%94%AF%E4%BB%98%E5%A4%B1%E8%B4%A5%E6%97%A0%E5%AD%97%E4%BD%93.png',

            ],
            txtBox:[
                {title:'等待处理',content:'已提交申请，等待银行处理'},
                {title:'支付成功',content:'所提交内容已成功完成'},
                {title:'无法完成操作',content:'提交失败，请重试'},
            ]
        }
    }
    render(){
        const divbody = {
            width:'100%',
            height:'100%',
            float:'left',
            background:'white'
        }
        return(
            <DocumentTitle title="结果页">
            <div style={divbody}>
                <div className="v_center" style={{width:'100%',height:'50%'}}>
                    <div className="h_center" style={{
                        width:'110px',
                        height:'90px',
                        backgroundImage:`url(${this.state.imgBox[this.state.status]})`,
                        backgroundSize:'100% 100%',
                        backgroundRepeat:'no-repeat',
                        backgroundPosition:'50% 50%',
                    }}/>
                    <div style={{width:'100%',height:'auto',float:'left',textAlign:'center',marginTop:'4%',fontWeight:'bold',color:'#373737'}}>
                        <span style={{fontSize:'1.3em'}}>{this.state.txtBox[this.state.status].title}</span><br/>
                        <span style={{fontSize:'1.1em'}}>{this.state.txtBox[this.state.status].content}</span>
                    </div>
                </div>
            </div> 
            </DocumentTitle>
        )
    }
}
