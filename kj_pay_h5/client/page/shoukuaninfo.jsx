import React, { Component } from 'react';
import {Link} from "react-router-dom";
import { Toast} from 'antd-mobile';
import $ from 'jquery';
import './mypay.css'
import DocumentTitle from 'react-document-title'
import { UserContext } from '../userContext';
import PropTypes from 'prop-types'

var num = 0;
var clock = null;
class Shoukuaninfo extends Component{
    
    constructor(props){
        super(props)
        
        console.log(this.bodyHeight)
        this.state={
            id:props.userInfo.userid,
            bodyHeight:document.documentElement.clientHeight,
            cardPhone:'4165',
            eleHeight:document.documentElement.clientHeight
        }
    }
    componentDidMount(){     
        
        console.log('componentDidMount')
        const bodyHei = `${document.documentElement.clientHeight}px`
        this.setState({
            bodyHeight:bodyHei,
            eleHeight:`${document.documentElement.clientHeight*0.38}px`
        })
    }
    clickgetVerify = () => {

        num = 10;
        $('[cgy=获取验证码]').css("pointer-events","none")
        
        clock = window.setInterval(function(){  

        $('[cgy=获取验证码]').find('span').text('重新获取(' + num  + ')')
            if(num > 0){
                num--;
                console.log(num)
            }
            else {
                //num = 15;
                console.log(clock)
                window.clearInterval(clock);
                $('[cgy=获取验证码]').find('span').text('获取验证码')
                $('[cgy=获取验证码]').css("pointer-events","auto")//启用div事件
            }
        },1000)
    }
    clickSurePay = () => {
        
        // var phone = $('[cgy=手机号]').val();
        // var pwd = $('[cgy=密码]').val();
        // var verify_code = $('[cgy=验证码]').val();
        
        window.clearInterval(clock);

        if(num > 0){
            Toast.success('提交成功', 2,()=>{
                this.context.router.history.push('./resultAction')
            });
        }else{
            Toast.success('验证码超时', 2);
        } 
    }
  
    render(){
        
        return (
            <DocumentTitle title="收款信息">
            <div style={{
                    width:'100%',
                    height:`${this.state.bodyHeight}`,
                    float:'left',
                }}>
                <div style={divStyle1}>
                    <span className="h_center v_center" style={spanStyle1}>填 写 银 行 卡 的 信 息</span>
                </div>
                <div style={divStyle2}>
                    <div style={divimgBox}>
                        <div class="center" style={divimg}/>
                    </div>
                    <div style={divimgInfo}>
                        <div style={divtxt1}>
                            <span className="v_center">工商银行</span>
                        </div>
                        <div style={divtxt2}>
                            <span className="v_center">尾号 {this.state.cardPhone} 储蓄卡</span>
                        </div>
                    </div>
                </div>
                <div style={divStyle3}>
                    <span className="v_center" style={spanStyle1}>请使用{owntxt()}进行支付，支付成功后不可更改</span>
                </div>
                <div style={{width:'100%',
                    height:`${this.state.eleHeight}`,
                    float:'left',
                    background:'white',}}>
                    <div style={divBox}>
                        <div style={divLabel}>
                                <div className="h_center v_center" style={icon1}/>
                        </div>
                        <div style={divInput}>
                            <input type="text" style={inputStyle} placeholder="*成伟"/>
                        </div>
                    </div>
                    <div style={divBox}>
                        <div style={divLabel}>
                            <div className="h_center v_center" style={icon2}/>
                        </div>
                        <div style={divInput}>
                            <input type="text" style={inputStyle} placeholder="32058**********516"/>
                        </div>
                    </div>
                    <div style={divBox}>
                        <div style={divLabel}>
                            <div className="h_center v_center" style={icon3}/>
                        </div>
                        <div style={divInput}>
                            <input type="text" style={inputStyle} placeholder="银行预留手机号"/>
                        </div>
                    </div>
                    <div style={divBox}>
                        <div style={divLabel}>
                            {/* <span className="v_center">验&#8194;证&#8194;码</span> */}
                            <div className="h_center v_center" style={icon4}/>
                        </div>
                        <div style={divInput}>
                            <input type="text" style={inputStyle} placeholder="短信验证码"/>
                        </div>
                        <div cgy="获取验证码" style={verifyBtn} onClick={this.clickgetVerify}>
                            <span  className="v_center">获取验证码</span>
                        </div>
                    </div>
                </div>
                <div style={divStyle5}>
                    <div style={divtxt3}>
                        <div className="v_center" style={{width:'auto',height:'auto',float:'left',marginLeft:15,marginTop:15}}>
                            <div className="v_center" style={divIcon}/>
                            <div className="v_center" style={{width:'auto',height:'auto',float:'left'}}><span  style={spanStyle2}>同意服务协议</span></div>
                        </div>
                        
                    </div>
                    <div style={divtxt4}>
                        <span className="v_center" style={spanStyle3}>限额提示：20000元/笔；200000元/日；500000元/月</span>
                    </div>
                    <div style={divtxt4}>
                        <span className="v_center" style={spanStyle3}>具体限额以银行卡参考为准</span>
                    </div>
                    <div style={divbtnBox}>
                        <div class="h_center v_center" style={divBtn} onClick={this.clickSurePay}>
                            <span className="v_center">确认支付</span>
                        </div>
                    </div>
                </div>
            </div>
            </DocumentTitle>        
        )
    }
}
const ShoukuaninfoConsumer = ({ }) => (
    <UserContext.Consumer>
        {user => {
            return (
                <Shoukuaninfo
                    userInfo={user.userInfo}
                />
            )
        }}
    </UserContext.Consumer>
)
export default ShoukuaninfoConsumer
Shoukuaninfo.contextTypes = {
    router: PropTypes.object.isRequired
}
const divStyle1 = {
    width:'100%',
    height:'4.5%',
    float:'left',
    textAlign:'center'
}
const divStyle2 = {
    width:'100%',
    height:'10%',
    float:'left',
    background:'white'
}
const divStyle3 = {
    width:'100%',
    height:'5%',
    float:'left',
    textAlign:'center'
}
const divStyle4 = {
    width:'100%',
    //height:'38%',
    height:`${document.documentElement.clientHeight*0.3}px`,
    float:'left',
    background:'white',
}
const divStyle5 = {
    width:'100%',
    height:'39%',
    float:'left',
}
const spanStyle1 = {
    color:'gray',
    fontSize:'0.9em'
}
const owntxt = () => (
    <span style={{color:'#2e76ff'}}>本人信息</span>
)
const divimgBox = {
    width:'15%',
    height:'100%',
    float:'left',
    marginLeft:'20px',
}
const divimgInfo = {
    width:'auto',
    height:'100%',
    float:'left'
}
const divimg = {
    width:'40px',
    height:'40px',
    float:'left',
    background:'url(http://pfnew9fvk.bkt.clouddn.com/%E5%B7%A5%E5%95%86%E9%93%B6%E8%A1%8C%E5%9B%BE%E6%A0%87.jpg)',
    backgroundSize:'100% 100%',
    backgroundRepeat:'no-repeat',
    backgroundPosition:'0% 0%',
}
const divtxt1 = {
    width:'100%',
    height:'55%',
    float:'left',
    fontSize:'1.5em'
}
const divtxt2 = {
    width:'100%',
    height:'45%',
    float:'left',
    color:'gray',
    fontSize:'1.1em'
}
const divBox = {
    width:'100%',
    height:'25%',
    float:'left',
    boxSizing:'border-box',
    position:'relative',
    borderBottom:'1px solid #dfdfdf'
}
const divLabel = {
    width:'18%',
    height:'100%',
    float:'left',
}
const divInput = {
    width:'82%',
    height:'100%',
    float:'left',
    fontSize:'1.2em'
}
const inputStyle = {
    border:'0px',
    width:'100%',
    height:'100%',
    padding:'0px'
}
const verifyBtn = {
    width:'auto',
    height:'60%',
    position:'absolute',
    right:'0%',
    top:'20%',
    color:'#2e76ff',
    fontSize:'1.2em',
    marginRight:'5px',
    border:'1px solid #2e76ff',
    borderRadius:'8px',
    paddingLeft:'3px',
    paddingRight:'3px'
}
const divtxt3 = {
    width:'100%',
    height:'20%',
    float:'left'
}
const divtxt4 = {
    width:'100%',
    height:'10%',
    float:'left'
}
const spanStyle2 = {
    marginTop:'10px',
    marginLeft:'3px',
    fontSize:'0.9em',
    color:'gray'
}
const spanStyle3 = {
    marginLeft:'15px',
    fontSize:'0.8em',
    color:'gray'
}
const divbtnBox = {
    width:'100%',
    height:'50%',
    float:'left'
}
const divIcon = {
    width:'16px',
    height:'16px',
    float:'left',
    background:'url(http://pfnew9fvk.bkt.clouddn.com/%E7%BB%BF%E8%89%B2%E7%AE%AD%E5%A4%B4%E7%A1%AE%E8%AE%A4.jpg)',
    backgroundSize:'100% 100%',
    backgroundRepeat:'no-repeat',
    backgroundPosition:'0% 0%',

}
const divBtn = {
    width:'92%',
    height:'2.3em',
    color:'white',
    fontSize:'1.15em',
    background:'#2e76ff',
    textAlign:'center',
    borderRadius:'25px'
}
const icon1 = {
    width:'19px',
    height:'22px',
    backgroundImage:'url(http://pfnew9fvk.bkt.clouddn.com/%E5%A7%93%E5%90%8D.png)',
    backgroundSize:'100% 100%',
    backgroundPosition:'0% 0%',
    backgroundRepeat:'no-repeat'
}
let icon2 = {...icon1};
let icon3 = {...icon1};
let icon4 = {...icon1};
icon2.backgroundImage = 'url(http://pfnew9fvk.bkt.clouddn.com/%E8%BA%AB%E4%BB%BD%E8%AF%81%E5%8F%B7.png)';
icon3.backgroundImage = 'url(http://pfnew9fvk.bkt.clouddn.com/%E6%89%8B%E6%9C%BA%E5%8F%B7.png)';
icon4.backgroundImage = 'url(http://pfnew9fvk.bkt.clouddn.com/%E9%AA%8C%E8%AF%81%E7%A0%81%E9%93%B6%E8%A1%8C%E5%8D%A1%E4%BF%A1%E6%81%AF%E9%A1%B5%E9%9D%A2.png)';