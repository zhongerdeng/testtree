import React, { Component } from 'react';
import './mypay.css'
import QrCode from 'qrcode.react'
import DocumentTitle from 'react-document-title'
import { UserContext } from '../userContext';

var index = 0;
class Promote extends Component{
    constructor(props){
        super(props)
        this.state={
            id:props.userInfo.userid,
            img:[
                'http://pfn1eg091.bkt.clouddn.com/20180926085742.jpg',
                'http://pfn1eg091.bkt.clouddn.com/20180926092456.jpg',
                'http://pfn1eg091.bkt.clouddn.com/20180926092740.jpg',
                'http://pfn1eg091.bkt.clouddn.com/20180926092758.jpg',
            ],
            imgIndex:0,
            visible:'visible',
            codeVisible:'hidden'
        }
    }
    forwardImg = () => {
        if(index < 3)index++;
        this.setState({
            imgIndex:index
        })
    }
    backImg = () => {
        if(index > 0)index--;
        this.setState({
            imgIndex:index
        })
    }
    genCode = () => {
       this.setState({
           visible:'hidden',
           codeVisible:'visible'
       })
       console.log('gencode')
    }
    render(){
        return(
            <DocumentTitle title="推广二维码">
            <div style={divbody}>
                <div style={{
                    width:'100%',
                    height:'100%',
                    float:'left',
                    backgroundImage:`url(${this.state.img[index]})`,
                    backgroundSize:'100% 100%',
                    backgroundPosition:'50% 50%',
                    backgroundRepeat:'no-repeat'
                }}>
                <div style={{
                    width:'100px',
                    height:'100px',
                    float:'left',
                    visibility:`${this.state.codeVisible}`,
                    position:"absolute",
                    left:'60%',
                    top:'80%'
                }}>
                    <QrCode 
                        value="https://api.weixin.qq.com/cgi-bin/token?grant_type=electricaldragon&appid=wx33132a5f5985925d&secret=92da92dc9f88a12caade342858d78057"
                        size="128"
                        style={{width:'100%',height:'100%'}}
                    />
                </div>
                    <div style={{width:'100%',height:'100%',float:'left',visibility:`${this.state.visible}`}}>
                        <div style={div_part1}>
                            <div style={{width:'12%',height:'100%',float:'left'}}>
                                <div className="h_center v_center" style={back} onClick={()=>{this.backImg()}}/>
                            </div>
                            <div style={{width:'76%',height:'98%',float:'left'}}>
                                <div className="h_center v_center" style={{
                                    width:'100%',
                                    height:'92%',
                                    float:'left',
                                    backgroundImage:`url(${this.state.img[this.state.imgIndex]})`,
                                    backgroundSize:'100% 100%',
                                    backgroundPosition:'50% 50%',
                                    backgroundRepeat:'no-repeat'
                                }}
                                />
                            </div>
                            <div style={{width:'12%',height:'100%',float:'left'}}>
                                <div className="h_center v_center" style={forward} onClick={()=>{this.forwardImg()}}/>
                            </div>
                        </div>
                        <div style={div_part2}>
                            <div className="h_center v_center" style={div_btn}>
                                <span className="v_center" style={{color:'white'}} onClick={()=>{this.genCode()}}>生成推广二维码</span>
                            </div>
                        </div>
                    </div>   
                </div> 
            </div>
            </DocumentTitle>
        )
    }
}
const PromoteConsumer = ({ }) => (
    <UserContext.Consumer>
        {user => {
            return (
                <Promote
                    userInfo={user.userInfo}
                />
            )
        }}
    </UserContext.Consumer>
)
export default PromoteConsumer

const divbody = {
    width:'100%',
    height:'100%',
    float:'left',
    background:'#f1f1f1'
}
const forward = {
    width:'40px',
    height:'40px',
    backgroundColor:'gray',
    backgroundImage:'url(http://pfnew9fvk.bkt.clouddn.com/forward.jpg)',
    backgroundSize:'100% 100%',
    backgroundPosition:'50% 50%',
    backgroundRepeat:'no-repeat',
    borderRadius:'50%',
}
const back = {
    width:'40px',
    height:'40px',
    backgroundColor:'gray',
    backgroundImage:'url(http://pfnew9fvk.bkt.clouddn.com/back.jpg)',
    backgroundSize:'100% 100%',
    backgroundPosition:'50% 50%',
    backgroundRepeat:'no-repeat',
    borderRadius:'50%',
}
const div_part1 = {
    width:'100%',
    height:'80%',
    float:'left',
    background:'black'
}
const div_part2 = {
    width:'100%',
    height:'20%',
    float:'left',
    background:'black'
}

const div_btn = {
    width:'80%',
    height:'2.6em',
    borderRadius:'25px',
    textAlign:'center',
    background:'#1E90FF'
}