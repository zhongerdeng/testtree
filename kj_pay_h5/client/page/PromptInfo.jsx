import React, { Component } from 'react';
import './mypay.css'
import DocumentTitle from 'react-document-title'

export default class PromptInfo extends Component{
    constructor(props){
        super(props)
        this.state={
            history:props.history,
            title:props.title,
            pmtInfoIndex:props.pmtInfoIndex,
            prompInfoBox:[
                (<span style={{fontSize:'1.1em'}}>您已完成注册，赶紧去交易吧</span>),
                (<span style={{fontSize:'1.1em'}}>您已通过资质审核，赶紧去交易吧</span>),
                (<span style={{fontSize:'1.1em'}}>您还没有小伙伴<br/>请赶快<font style={{color:'#2e76ff'}} onClick={()=>{this.state.history.push('./promote')}}>&#8194;喊小伙伴吧&#8194;</font></span>),
                (<span style={{fontSize:'1.1em'}}>您还没有达标返现用户<br/>请赶快喊小伙伴交易返现吧</span>),
                (<span style={{fontSize:'1.1em'}}>您还没有已结算的订单记录<br/>亲，赶快<font style={{color:'#2e76ff'}} onClick={()=>{this.state.history.push('./shoukuan')}}>&#8194;快捷收款&#8194;</font>吧</span>),
                (<span style={{fontSize:'1.1em'}}>您还没有未结算的订单记录<br/>亲，赶快<font style={{color:'#2e76ff'}} onClick={()=>{this.state.history.push('./shoukuan')}}>&#8194;快捷收款&#8194;</font>吧</span>),
            ]
        }
    }
    render(){
        return(
            <DocumentTitle title={this.state.title}>
                <div style={body}>
                    <div className="v_center" style={{width:'100%',height:'50%',float:'left'}}>
                        <div className="h_center" style={{
                            width:'80px',
                            height:'100px',
                            background:'url(http://pfnew9fvk.bkt.clouddn.com/%E9%BB%98%E8%AE%A4%E5%9B%BE%E6%A0%87.jpg)',
                            backgroundSize:'100% 100%',
                            backgroundRepeat:'no-repeat',
                            backgroundPosition:'50% 50%' 
                        }}
                        />
                        <div style={{textAlign:'center',marginTop:'15px'}}>
                            {this.state.prompInfoBox[this.state.pmtInfoIndex]}
                        </div>
                    </div>
                </div>
            </DocumentTitle>
        )
    }
}
const body = {
    width:'100%',
    height:'100%',
    float:'left',
   // background:'#f1f1f1'
}