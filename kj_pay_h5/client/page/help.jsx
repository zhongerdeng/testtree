import React, { Component } from 'react';
import './mypay.css'
import DocumentTitle from 'react-document-title'

export default class Promote extends Component{
    constructor(props){
        super(props)
        this.state={
            questionPageShow:'hidden',
            questionNavShow:'visible',
            bgImg:'',
            bgImgBox:[
               'http://pfnew9fvk.bkt.clouddn.com/%E5%B8%AE%E5%8A%A9%E4%B8%AD%E5%BF%83_%E6%96%B0%E6%89%8B%E5%BF%85%E7%9C%8B.jpg',
               'http://pfnew9fvk.bkt.clouddn.com/%E4%BA%A4%E6%98%93%E9%97%AE%E9%A2%98.jpg',
               'http://pfnew9fvk.bkt.clouddn.com/%E7%BB%93%E7%AE%97%E9%97%AE%E9%A2%98.jpg',
               'http://pfnew9fvk.bkt.clouddn.com/%E9%BC%93%E5%8A%B1%E9%87%91%E9%97%AE%E9%A2%98.jpg',
               'http://pfnew9fvk.bkt.clouddn.com/%E5%B8%AE%E5%8A%A9%E4%B8%AD%E5%BF%83_%E8%81%94%E7%B3%BB%E5%AE%A2%E6%9C%8D.jpg'

            ]
        }
    }
    componentDidMount(){
        console.log('help')
    }
    navClick = (index) => {

        this.setState({
            questionPageShow:'visible',
            questionNavShow:'hidden',
            bgImg:this.state.bgImgBox[index]
        })
        console.log(index)
    }
    render(){
        return(
            <DocumentTitle title="帮助中心">
            <div style={divbody}>
                <div style={{
                    width:'100%',
                    height:'100%',
                    float:'left',
                    visibility:`${this.state.questionPageShow}`,
                    backgroundImage:`url(${this.state.bgImg})`,
                    backgroundSize:'100% 100%',
                    backgroundRepeat:'no-repeat',
                    backgroundPosition:'50% 50%',
                    }}>
                    <div style={{
                        width:'100%',
                        height:'100%',
                        float:'left',
                        visibility:`${this.state.questionNavShow}`,
                    }}>
                        <div style={divPart_1}>
                            <div style={{
                                width:'100%',
                                height:'100%',
                                backgroundImage:'url(http://pfnew9fvk.bkt.clouddn.com/%E5%B8%AE%E5%8A%A9%E4%B8%AD%E5%BF%83.jpg)',
                                backgroundSize:'100% 100%',
                                backgroundRepeat:'no-repeat',
                                backgroundPosition:'50% 50%',
                            }}/>
                        </div>
                        <div style={divPart_2}>
                            <span className="v_center">&#8195;分类问题</span>
                        </div>
                        <div style={divPart_3}>
                            <div style={divPart_3_div_box} onClick={()=>{this.navClick(0)}}>
                                <div style={divPart_3_div_box_part_1}>
                                    <div className="h_center v_center" style={{
                                        width:'25px',
                                        height:'25px',
                                        backgroundImage:'url(http://pfnew9fvk.bkt.clouddn.com/%E6%96%B0%E6%89%8B.png)',
                                        backgroundSize:'100% 100%',
                                        backgroundRepeat:'no-repeat',
                                        backgroundPosition:'50% 50%',
                                    }}/>
                                </div>
                                <div style={divPart_3_div_box_part_2}>
                                    <div className="v_center">
                                        <span style={{fontSize:'1.1em'}}>[1]新手必看</span><br/>
                                        <span style={{color:'#b1b1b1',fontSize:'0.7em'}}>适合小白用户，必看</span>
                                    </div>   
                                </div>
                            </div>
                            <div style={divPart_3_div_box} onClick={()=>{this.navClick(1)}}>
                                <div style={divPart_3_div_box_part_1}>
                                    <div className="h_center v_center" style={{
                                        width:'25px',
                                        height:'25px',
                                        backgroundImage:'url(http://pfnew9fvk.bkt.clouddn.com/%E4%BA%A4%E6%98%93.png)',
                                        backgroundSize:'100% 100%',
                                        backgroundRepeat:'no-repeat',
                                        backgroundPosition:'50% 50%',
                                    }}/>
                                </div>
                                <div style={divPart_3_div_box_part_2}>
                                    <div className="v_center">
                                        <span style={{fontSize:'1.1em'}}>[2]交易问题</span><br/>
                                        <span style={{color:'#b1b1b1',fontSize:'0.7em'}}>刷卡时间，额度</span>
                                    </div>  
                                </div>
                            </div>
                            <div style={divPart_3_div_box} onClick={()=>{this.navClick(2)}}>
                                <div style={divPart_3_div_box_part_1}>
                                    <div className="h_center v_center" style={{
                                        width:'25px',
                                        height:'25px',
                                        backgroundImage:'url(http://pfnew9fvk.bkt.clouddn.com/%E7%BB%93%E7%AE%97.png)',
                                        backgroundSize:'100% 100%',
                                        backgroundRepeat:'no-repeat',
                                        backgroundPosition:'50% 50%',
                                    }}/>
                                </div>
                                <div style={divPart_3_div_box_part_2}>
                                    <div className="v_center">
                                        <span style={{fontSize:'1.1em'}}>[3]结算问题</span><br/>
                                        <span style={{color:'#b1b1b1',fontSize:'0.7em'}}>到账时间，不到账查...</span>
                                    </div>  
                                </div>
                            </div>
                            <div style={divPart_3_div_box} onClick={()=>{this.navClick(3)}}>
                                <div style={divPart_3_div_box_part_1}>
                                    <div className="h_center v_center" style={{
                                        width:'25px',
                                        height:'25px',
                                        backgroundImage:'url(http://pfnew9fvk.bkt.clouddn.com/%E9%BC%93%E5%8A%B1%E9%87%91.png)',
                                        backgroundSize:'100% 100%',
                                        backgroundRepeat:'no-repeat',
                                        backgroundPosition:'50% 50%',
                                    }}/>
                                </div>
                                <div style={divPart_3_div_box_part_2}>
                                    <div className="v_center">
                                        <span style={{fontSize:'1.1em'}}>[4]赚鼓励金</span><br/>
                                        <span style={{color:'#b1b1b1',fontSize:'0.7em'}}>工作赚钱两不误</span>
                                    </div>  
                                </div>
                            </div>
                            <div style={divPart_3_div_box} onClick={()=>{this.navClick(4)}}>
                                <div style={divPart_3_div_box_part_1}>
                                    <div className="h_center v_center" style={{
                                        width:'25px',
                                        height:'25px',
                                        backgroundImage:'url(http://pfnew9fvk.bkt.clouddn.com/%E5%AE%A2%E6%9C%8D.png)',
                                        backgroundSize:'100% 100%',
                                        backgroundRepeat:'no-repeat',
                                        backgroundPosition:'50% 50%',
                                    }}/>
                                </div>
                                <div style={divPart_3_div_box_part_2}>
                                    <div className="v_center">
                                        <span style={{fontSize:'1.1em'}}>[5]联系客服</span><br/>
                                        <span style={{color:'#b1b1b1',fontSize:'0.7em'}}>获取微信联系方式</span>
                                    </div>  
                                </div>
                            </div>
                        </div>
                    </div>
                </div>    
            </div>
            </DocumentTitle>
        )
    }
}
const divbody = {
    width:'100%',
    height:'100%',
    float:'left',
    background:'#f1f1f1',
}
const divPart_1 = {
    width:'100%',
    height:'23%',
    float:'left',
}
const divPart_2 = {
    width:'100%',
    height:'7%',
    float:'left',
}
const divPart_3 = {
    width:'100%',
    height:'40%',
    float:'left',
    background:'white'
}
const divPart_3_div_box = {
    width:'50%',
    height:'33.3%',
    float:'left',
    outline:'1px solid #f1f1f1'
}
const divPart_3_div_box_part_1 = {
    width:'30%',
    height:'100%',
    float:'left',
}
const divPart_3_div_box_part_2 = {
    width:'70%',
    height:'100%',
    float:'left',
}
