import React, { Component } from 'react';
import ReactDOM from 'react-dom'
import { Tabs, Badge, ListView, PullToRefresh, Modal } from 'antd-mobile';
import './mypay.css'
import DocumentTitle from 'react-document-title'
import queryData from '../action/request'
import PromptInfo from './PromptInfo'
import { UserContext } from '../userContext';
import PropTypes from 'prop-types'

const tabs = [
    { title: <Badge >已认证</Badge> },
    { title: <Badge >未认证</Badge> },
    { title: <Badge >达标返现</Badge> },
  ]

class EncourageMoney extends Component{

    constructor(props){
        super(props)
        const dataSource = new ListView.DataSource({
            rowHasChanged: (row1, row2) => row1 !== row2,
        });
        this.state={
            id:props.userInfo.userid,
            money:'0.00',
            tabIndex:0,
            down: true,
            height:'100%',
            divHeight:'100%',
            dataSource,
            refreshing: true,
            isLoading: false,
            useBodyScroll: false,
            tabIndexStr:['已认证','未认证','达标返现'],
            defaultTxt:['您还没有已认证的记录','您没有未认证的记录','您没有达标反现的记录'],
            promptIndex:[2,2,3],
            interface:[
                '/user//identifyUserList?_xyz',
                '/user//identifyUserList?_xyz',
                '/recommission/sUserCommissionList?_xyz',
            ],
        
        }
        this.listData = [
            {dataArr:[],pIndex:1,count:0},
            {dataArr:[],pIndex:1,count:0},
            {dataArr:[],pIndex:1,count:0},
        ],
        this.comDidMount = false;
        this.withdrawalMoney = 0;
    }

    componentDidUpdate() {

        if (this.state.useBodyScroll) {
          document.body.style.overflow = 'auto';
        } else {
          document.body.style.overflow = 'hidden';
        }
    }
    componentDidMount(){
        console.log(this.props.userInfo.openid)
        console.log(this.props.userInfo.userid)

        let params = {
            userId:this.state.id
        }
        queryData('/recommission/getCommission?_xyz',params).then((data)=>{
            this.setState({money:data})
        })
        const hei =  `${document.documentElement.clientHeight-ReactDOM.findDOMNode(this.refs.mytab).clientHeight}px`
        this.setState({
            divHeight:hei,
        })
        this.getOrderList(this.state.tabIndex);
        this.comDidMount = true;
    }
    getOrderList = (status) => {

        let params;
        switch(status){
            case 0 :
                params = {
                    id : this.state.id,
                    status : `${this.state.tabIndexStr[status]}`,
                    page : this.listData[status].pIndex,
                    size : 10,
                }
            break;
            case 1 :
                params = {
                    id : this.state.id,
                    status : `${this.state.tabIndexStr[status]}`,
                    page : this.listData[status].pIndex,
                    size : 10,
                }
            break;
            case 2 :
                params = {
                    userId:this.state.id,  
                    page:this.listData[status].pIndex,  
                    size:10
                }
            break;
        }
        queryData(`${this.state.interface[status]}`,params).then((data)=>{
            console.log(data)
            if(data.count > 0){

                let tmpArr = [];
                for (let i = 0; i < data.list.length; i++) {

                    switch(status){
                        case 0 : tmpArr.push(
                            <div style={{width:'100%',height:'100%',float:"left"}}>               
                                <div style={{width:'33.3%',height:'100%',float:"left",textAlign:'left'}}>
                                    <div className="v_center">
                                        <span style={{fontSize:'0.9em'}}>&#8195;下级昵称{`${(this.listData[status].pIndex-1)*10+i}:${data.list[i].id}`}</span><br/>
                                        <span style={{fontSize:'0.9em'}}>&#8195;{data.list[i].aliasName}</span><br/>
                                    </div>
                                </div>
                                <div style={{width:'33.3%',height:'100%',float:"left",textAlign:'center'}}>
                                    <div className="v_center">
                                        <span style={{fontSize:'0.9em'}}>累计交易数量&#8195;</span><br/>
                                        <span style={{fontSize:'0.9em'}}>{data.list[i].countNum}&#8195;</span><br/>   
                                    </div> 
                                </div> 
                                <div style={{width:'33.3%',height:'100%',float:"left",textAlign:'right'}}>
                                    <div className="v_center">
                                        <span style={{fontSize:'0.9em'}}>注册时间&#8195;</span><br/>
                                        <span style={{fontSize:'0.9em'}}>{data.list[i].createAt}&#8195;</span><br/>     
                                    </div> 
                                </div> 
                            </div>
                        ); break;
                        case 1 : tmpArr.push(
                            <div style={{width:'100%',height:'100%',float:"left"}}>               
                                <div style={{width:'50%',height:'100%',float:"left",textAlign:'left'}}>
                                    <div className="v_center">
                                        <span style={{fontSize:'1em'}}>&#8195;下级昵称</span><br/>
                                        <span style={{fontSize:'0.9em'}}>&#8195;{data.list[i].aliasName}</span><br/>
                                    </div>
                                </div>
                                <div style={{width:'50%',height:'100%',float:"left",textAlign:'right'}}>
                                    <div className="v_center">
                                        <span style={{fontSize:'1em'}}>注册时间&#8195;</span><br/>
                                        <span style={{fontSize:'0.9em'}}>&#8195;{data.list[i].createAt}&#8195;</span><br/>     
                                    </div> 
                                </div> 
                            </div>
                        ); break;
                        case 2 : tmpArr.push(
                            <div style={{width:'100%',height:'100%',float:"left"}}>               
                                <div style={{width:'60%',height:'80%',float:"left"}}>
                                    <span style={{fontSize:'1em'}}>&#8195;时&#8195;&#8195;间：{data.list[i].createAt}</span><br/>
                                    <span style={{fontSize:'1em'}}>&#8195;订&#8194;单&#8194;号：{(this.listData[status].pIndex-1)*10+i}{data.list[i].orderNo}</span><br/>
                                    <span style={{fontSize:'1em'}}>&#8195;下级昵称：{data.list[i].aliasName}</span><br/>
                                </div>
                                <div style={{width:'40%',height:'100%',float:"left",textAlign:'right'}}>
                                    <div className="v_center">
                                        <span style={{fontSize:'1.4em'}}>{data.list[i].reAmount}元&#8195;</span><br/>
                                        <span style={{fontSize:'1em'}}>{data.list[i].status}&#8195;</span><br/>        
                                    </div> 
                                </div> 
                            </div>
                        ); break;
                    } 
                }
                
                this.listData[status].dataArr = [...this.listData[status].dataArr ,...tmpArr]
                this.listData[status].count = data.count;
                this.setState({
                    refreshing:false,
                    isLoading:false,
                    dataSource:this.state.dataSource.cloneWithRows(this.listData[status].dataArr)
                })
            }
        })
        
    }  
    onEndReached = (event) => {

        if (this.state.isLoading && !this.state.hasMore) {
            return;
          }
         // console.log('reach end', event);
          

          this.setState({ isLoading: true });  
          if(this.listData[this.state.tabIndex].pIndex  <= (this.listData[this.state.tabIndex].count/10+1)){
              ++this.listData[this.state.tabIndex].pIndex
              this.getOrderList(this.state.tabIndex)
              console.log(this.listData[this.state.tabIndex].pIndex)
          } 

    };
    onRefresh = () => {
        this.setState({ refreshing: true, isLoading: true });
  
        if(this.listData[this.state.tabIndex].pIndex  <= (this.listData[this.state.tabIndex].count/10+1)){
            ++this.listData[this.state.tabIndex].pIndex
            this.getOrderList(this.state.tabIndex)
       }
    };
    showContent = (status) => {
        return this.listData[status].count > 0 ? 
        (<ListView
                ref={"myListView"}
                key={'0'}
                dataSource={this.state.dataSource}
                renderRow={(value)=>{
                    return (
                    <div style={{width:'100%',height:'70px',float:'left',marginBottom:5,borderBottom:'1px solid #f1f1f1'}}>
                    {value}
                    </div>)
                }}
                initialListSize={25}
                useBodyScroll={this.state.useBodyScroll}
                style={{height: this.state.height,border:0}}
                pullToRefresh={<PullToRefresh
                    refreshing={this.state.refreshing}
                    onRefresh={this.onRefresh}
                    down={false}
                    direction={'up'}
                    />}
                onEndReached={this.onEndReached}
                pageSize={5}
        />):(<PromptInfo title={'我的鼓励金'} pmtInfoIndex={this.state.promptIndex[status]} history={this.context.router.history}/>) 
    };
    withdrawal = () => {
        Modal.prompt(
            '鼓励金提现', 
            (   <span>本次可提现¥{this.state.money}元
                    {/* <font style={{fontSize:'0.7em',color:'#2e76ff'}} onClick={()=>{
                    }}>&#8194;全部提现
                    </font> */}
                </span>
            ),
            [
                { text: '取消' },
                { text: '提现', onPress: value => console.log(`输入的内容:${value}`) },
            ], 
            'default',
            null,
        )
    }
    render(){
        return (
            <DocumentTitle title="我的鼓励金">
            <div style={body}>
                <div style={divPart_1}>
                    <div style={divCardImg}>
                        <div style={{
                            width:'auto',
                            height:'auto',
                            position:'absolute',
                            left:'8%',
                            top:'14%',
                        }}>
                            <span style={{color:'white',fontSize:'1.2em'}}>鼓励金</span>
                        </div>
                        <div style={{
                            width:'auto',
                            height:'auto',
                            position:'absolute',
                            left:'8%',
                            top:'32%',
                        }}>
                            <span style={{color:'white',fontSize:'1.5em'}}>{this.state.money}元</span>
                        </div>
                        <div style={{
                            width:'auto',
                            height:'2em',
                            position:'absolute',
                            left:'8%',
                            top:'62%',
                            border:'1px solid white',
                            borderRadius:'8px',
                            paddingLeft:'13px',
                            paddingRight:'13px',
                            textAlign:'center'
                        }} onClick={this.withdrawal}>
                            <span className="v_center" style={{color:'white',fontSize:'1.2em'}}>提现</span>
                        </div>
                    </div>
                </div>

                <div style={divPart_2}>
                    <div className="v_center" style={icon_1}/>
                    <div className="v_center" style={{width:'auto',height:'auto',float:'left'}}>
                        <span  style={{color:'#606060',fontSize:'0.8em'}}>&#8194;如何获得更高收益</span>
                    </div>
                </div>

                <div style={divPart_3}>
                    <span className="v_center" style={{fontSize:'1em',color:'#606060'}}>&#8195;&#8194;我的小伙伴</span>
                </div>
                <div style={divPart_4}>
                    <Tabs
                        ref={"mytab"} 
                        tabs={tabs}
                        initialPage={0}
                        onChange={(tab, index) => {
                            
                            this.listData[index].dataArr = [];
                            this.listData[index].pIndex = 1;
                            this.listData[index].count = 0;
                            this.setState({tabIndex:index})
                            this.getOrderList(index)
                        }}
                        onTabClick={(tab, index) => {
                            
                            this.listData[index].dataArr = [];
                            this.listData[index].pIndex = 1;
                            this.listData[index].count = 0;
                            this.setState({tabIndex:index})
                            this.getOrderList(index)
                        }}>
                        {this.showContent(this.state.tabIndex)}
                        
                    </Tabs>  
                    
                </div>
                
            </div>
            </DocumentTitle>
        )
    }
}
const EncourageMoneyConsumer = ({ }) => (
    <UserContext.Consumer>
        {user => {
            return (
                <EncourageMoney
                    userInfo={user.userInfo}
                />
            )
        }}
    </UserContext.Consumer>
)
export default EncourageMoneyConsumer
EncourageMoney.contextTypes = {
    router: PropTypes.object.isRequired
}

const body = {
    width:'100%',
    height:'100%',
    float:'left',
    background:'white',
}
const divPart_1 = {
    width:'100%',
    height:'24%',
    float:'left',
    background:'white'
}
const divPart_2 = {
    width:'100%',
    height:'5%',
    float:'left',
    background:'#f1f1f1'
}
const divPart_3 = {
    width:'100%',
    height:'5%',
    float:'left',
    background:'white'
}
const divPart_4 = {
    width:'100%',
    height:'66%',
    float:'left',
}

const divCardImg = {
    width:'100%',
    height:'100%',
    position:'relative',
    background:'url(http://pfnew9fvk.bkt.clouddn.com/%E9%BC%93%E5%8A%B1%E9%87%91%E8%83%8C%E6%99%AF%20_1.jpg)',
    backgroundSize:'100% 100%',
    backgroundPosition:'50% 50%',
    backgroundRepeat:'no-repeat'
}
const icon_1 = {
    width:16,
    height:16,
    float:'left',
    background:'url(http://pfnew9fvk.bkt.clouddn.com/%E8%AD%A6%E5%91%8A.png)',
    backgroundSize:'100% 100%',
    backgroundPosition:'0% 0%',
    backgroundRepeat:'no-repeat',
    marginLeft:'15px'
}