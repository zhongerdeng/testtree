import React, { Component } from 'react';
import ReactDOM from 'react-dom'
import './mypay.css'
import DocumentTitle from 'react-document-title'
import queryData from '../action/request'
import { UserContext } from '../userContext';

class OwnInfo extends Component{

    constructor(props){
        super(props)
        this.state={
            id:props.userInfo.userid,
            name:'',
            userId:'',
            phone:'',
            cardId:'',
            cardNumber:'',
            bankName:'',
            imgIndex:0,
            levelImg : [
                'http://pfnew9fvk.bkt.clouddn.com/%E6%99%AE%E9%80%9A.jpg',
                'http://pfnew9fvk.bkt.clouddn.com/%E9%BB%84%E9%87%91.jpg',
                'http://pfnew9fvk.bkt.clouddn.com/%E9%92%BB%E7%9F%B3.jpg',
            ]
        }
    }
    componentDidMount(){
       console.log(ReactDOM.findDOMNode(this.refs.bankCard).clientHeight) 
        let params = {
            id:this.state.id
        }
        queryData('/user/findOwnInfoById?_xyz',params).then((data)=>{
            console.log(data)
            const phoneStr = data.phone.substr(0,3) + ' **** ' + data.phone.substr(7,12)
            const cardIdStr = data.cardId.substr(0,5) + ' **** ' + data.cardId.substr(14,17)
            const cardNumStr = data.bankcardNum.substr(0,5) + ' **** **** ' + data.bankcardNum.substr(14,17)
            let tmIndex = 0;
            if(data.level == '普通合伙人')tmIndex = 0;
            else if(data.level == '黄金合伙人')tmIndex = 1;
            else if(data.level == '钻石合伙人')tmIndex = 2;
            this.setState({
                name:data.name,
                userId:data.id,
                phone:phoneStr,
                cardId:cardIdStr,
                cardNumber:cardNumStr,
                bankName:data.bankName,
                imgIndex:tmIndex
            })
        })
    }
    render(){
        
        return (
            <DocumentTitle title="我的信息">
            <div style={body}>
                <div style={divPart_1}>
                    <div ref={"bankCard"} className="h_center v_center" style={divCardImg}>
                        <div style={{
                            width:'auto',
                            height:'auto',
                            position:'absolute',
                            left:'7.5%',
                            top:'5.5%',
                        }}>
                            <span style={{color:'white',fontSize:'1.5em'}}>{this.state.bankName}</span>
                        </div>
                        <div style={{
                            width:'auto',
                            height:'auto',
                            position:'absolute',
                            left:'77%',
                            top:'5.5%',
                        }}>
                            <span style={{color:'white',fontSize:'1.5em'}}>结算卡</span>
                        </div>
                        <div style={{
                            width:'auto',
                            height:'auto',
                            position:'absolute',
                            left:'7.5%',
                            top:'55%',
                        }}>
                            <span style={{color:'white',fontSize:'1.4em'}}>{this.state.cardNumber}</span>
                        </div>
                        <div style={{
                            width:'auto',
                            height:'auto',
                            position:'absolute',
                            left:'77%',
                            top:'80%',
                        }}>
                            <span style={{color:'white',fontSize:'1.2em'}}>{this.state.name}</span>
                        </div>
                    </div>
                </div>
                <div style={divPart_2}>
                    <div className="h_center v_center" style={{width:'95%',height:"90%",background:'white',borderRadius:'7px'}}>
                        <div style={{width:'70%',height:'100%',float:'left'}}>
                            <div className="h_center v_center" style={{width:'auto',height:"auto",marginLeft:20}}>
                                <span  style={{fontSize:'1.3em',color:'#9b9b9b'}}>{this.state.name}</span><br/>
                                <span  style={{fontSize:'1em',color:'#3399FF'}}>已认证</span>
                            </div>
                        </div> 
                        <div style={{
                            width:'25%',
                            height:'100%',
                            float:'right',
                            backgroundImage:`url(${this.state.levelImg[this.state.imgIndex]})`,
                            backgroundSize:'100% 100%',
                            backgroundPosition:'50% 50%',
                            backgroundRepeat:'no-repeat',
                            borderRadius:'7px'
                        }} onClick={()=>{this.props.history.push('./upgrade')}}/>
                    </div>
                </div>

                <div style={divPart_3}>
                    <div style={divPart_3_divBox}>
                        <div style={{width:'50%',height:'100%',float:'left'}}>
                            <span className="v_center">&#8195;联&#8195;系&#8195;人</span>
                        </div>
                        <div style={{width:'50%',height:'100%',float:'left',textAlign:'right'}}>
                            <span className="v_center">{this.state.name}&#8195;</span>
                        </div>
                    </div>
                    <div style={divPart_3_divBox}>
                        <div style={{width:'50%',height:'100%',float:'left'}}>
                            <span className="v_center">&#8195;用&#8194;户&#8194;编&#8194;号</span>
                        </div>
                        <div style={{width:'50%',height:'100%',float:'left',textAlign:'right'}}>
                            <span className="v_center">{this.state.userId}&#8195;</span>
                        </div>
                    </div>
                    <div style={divPart_3_divBox}>
                        <div style={{width:'50%',height:'100%',float:'left'}}>
                            <span className="v_center">&#8195;手&#8194;机&#8194;号&#8194;码</span>
                        </div>
                        <div style={{width:'50%',height:'100%',float:'left',textAlign:'right'}}>
                            <span className="v_center">{this.state.phone}&#8195;</span>
                        </div>
                    </div>
                    <div style={divPart_3_divBox}>
                        <div style={{width:'50%',height:'100%',float:'left'}}>
                            <span className="v_center">&#8195;身&#8194;份&#8194;证&#8194;号</span>
                        </div>
                        <div style={{width:'50%',height:'100%',float:'left',textAlign:'right'}}>
                            <span className="v_center">{this.state.cardId}&#8195;</span>
                        </div>
                    </div>
                    <div style={divPart_3_divBox}>
                        <div style={{width:'50%',height:'100%',float:'left'}}>
                            <span className="v_center">&#8195;结&#8194;算&#8194;卡&#8194;号</span>
                        </div>
                        <div style={{width:'50%',height:'100%',float:'left',textAlign:'right'}}>
                            <span className="v_center">{this.state.cardNumber}&#8195;</span>
                        </div>
                    </div>
                </div>
                <div style={divPart_4}>
                    <div style={{widht:'100%',height:"99%",background:'white',marginTop:'1%'}}>
                        <div style={divPart_4_divBox}>
                            <div style={{width:'50%',height:'100%',float:'left'}}>
                                <span className="v_center">&#8195;套&#8194;现&#8194;费&#8194;率</span>
                            </div>
                            <div style={{width:'50%',height:'100%',float:'left',textAlign:'right'}}>
                                <span className="v_center">5.5‰+2.00&#8195;</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            </DocumentTitle>
        )
    }
}
const body = {
    width:'100%',
    height:'100%',
    float:'left',
    background:'#f1f1f1'
}
const divPart_1 = {
    width:'100%',
    height:'35%',
    float:'left',
    position:'relative',
    background:'white',
}
const divPart_2 = {
    width:'100%',
    height:'15%',
    float:'left',
    background:'#f1f1f1'
}
const divPart_3 = {
    width:'100%',
    height:'36%',
    float:'left',
    background:'white'
}
const divPart_4 = {
    width:'100%',
    height:'7%',
    float:'left',
    background:'#f1f1f1'
}

const divCardImg = {
    width:'90%',
    height:'90%',
    background:'url(http://pfnew9fvk.bkt.clouddn.com/%E7%BB%93%E7%AE%97%E5%8D%A1_2.jpg)',
    backgroundSize:'100% 100%',
    backgroundPosition:'0% 0%',
    backgroundRepeat:'no-repeat'
}
const divPart_3_divBox = {
    width:'100%',
    height:'20%',
    float:'left',
    fontSize:'1em',
    color:'#9b9b9b',
}
const divPart_4_divBox = {
    width:'100%',
    height:'100%',
    float:'left',
    fontSize:'1em',
    color:'#9b9b9b',
}
const OwnInfoConsumer = ({ }) => (
    <UserContext.Consumer>
        {user => {
            return (
                <OwnInfo
                    userInfo={user.userInfo}
                />
            )
        }}
    </UserContext.Consumer>
)
export default OwnInfoConsumer