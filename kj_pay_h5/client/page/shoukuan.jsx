import React, { Component } from 'react';
import ReactDom from 'react-dom'
import './mypay.css'
import DocumentTitle from 'react-document-title'
import { UserContext } from '../userContext';
import { Toast} from 'antd-mobile';
import PropTypes from 'prop-types'
import queryData from '../action/request'

class Shoukuan extends Component{
    constructor(props) {
        super(props);
        this.state = {
            id:props.userInfo.userid,
            bodyHeight:document.documentElement.clientHeight,
            userName : '',
            eleHeight:document.documentElement.clientHeight,
            
        };
      }
    componentDidMount(){  
        const bodyHei = `${document.documentElement.clientHeight}px`
        this.setState({
            bodyHeight:bodyHei,
            eleHeight:`${document.documentElement.clientHeight*0.92}px`
        })
        let params = {
            userId:this.state.id
        }
        queryData("/user/getUserByUserId?_xyz",params).then((data)=>{
            console.log(data)
            this.setState({
                userName:data
            })
        })
    }
    clickNext = () => {
        const money = ReactDom.findDOMNode(this.refs.money).value
        const cardNum = ReactDom.findDOMNode(this.refs.cardNum).value
        const phone = ReactDom.findDOMNode(this.refs.phone).value

        let params = {
            id:this.state.id,
            bankcardNum:cardNum,
            orderAmount:money,
            reservedPhone:phone
        }
       // console.log(params)
         
       queryData('/order/validateInfo?_xyz',params).then((data)=>{
           console.log(data)
            
            if(data.status==true){
                this.context.router.history.push("./shoukuaninfo");
            }else if(data.status==false){
                Toast.fail(data.message,1.5)
            }
        })
    }
    render(){
        
        return (
            <DocumentTitle title="快捷收款">
            <div style={{
                width:'100%',
                height:`${this.state.bodyHeight}`,
                float:'left',
                background:'#f1f1f1'
            }}>
                <div style={divPart1}/>
                <div style={divPart2}/>
                <div style={{
                    width:'92%',
                    height:`${this.state.eleHeight}`,
                    float:'left',
                    position:'absolute',
                    left:'4%',
                    top:'8%',
                }}>

                    <div style={divPart4}>
                        <div style={divPart5}>
                            <div style={userIcon}/>
                            <div style={{textAlign:'center'}}><span>{this.state.userName}</span></div>
                            <div style={divtxt}><span className="v_center">实时到账</span></div>
                        </div>
                        <div style={{width:'90%',height:"70%",float:'none',margin:'auto'}}>
                            <div style={divrow}>
                                <div className="v_center" style={icon1}/>
                                <div className="v_center" style={divInputBox}>  
                                    <input ref = "money" type="text" placeholder="请输入交易金额" style={divInput}/>
                                </div>
                            </div>
                            <div style={divrow}>
                                <div className="v_center" style={icon2}/>
                                <div className="v_center" style={divInputBox}> 
                                    <input ref = "cardNum" type="text" placeholder="请输入交易卡号" style={divInput}/>
                                </div>
                            </div>
                            <div style={divrow}>
                                <div className="v_center" style={icon3}/>
                                <div className="v_center" style={divInputBox}> 
                                    <input ref = "phone" type="text" placeholder="请输入银行卡预留手机号" style={divInput}/>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div style={{width:'100%',height:'2em',float:'left',marginTop:20}}>
                        <div className="h_center" style={{width:'80%',height:20,textAlign:'center'}}>
                            <div style={icon}/>
                            <div style={divSpanBox1} >
                                <span class="v_center" style={spanStyle}>交易卡必须和结算卡是同一持卡人</span>
                            </div>
                            {/* <div style={divSpanBox2} >
                                <span class="v_center" style={spanStyle}>交易限额说明</span>
                            </div> */}
                        </div>
                    </div>
                    <div style={{width:'100%',height:'auto',float:'left',marginTop:45}}>
                        <div className="center " style={divBtn} onClick={this.clickNext}>
                            <span className="v_center" style={{color:'white',fontSize:'1em'}}>下一步</span>
                        </div>
                    </div> 
                </div>
            </div>
            </DocumentTitle>
        )
    }
}
const ShoukuanConsumer = ({}) => (
    <UserContext.Consumer>
        {user => {
            return (
                <Shoukuan
                    userInfo={user.userInfo}
                />
            )
        }} 
    </UserContext.Consumer>
)
export default ShoukuanConsumer
Shoukuan.contextTypes = {
    router: PropTypes.object.isRequired
}
const divPart1 = {
    width:'100%',
    height:'30%',
    float:'left',
    background:'url(http://pfnew9fvk.bkt.clouddn.com/%E5%AE%9E%E6%97%B6%E5%88%B0%E8%B4%A6%E9%A1%B5%E9%9D%A2.png)',
    backgroundSize:'100% 100%',
    backgroundRepeat:'no-repeat',
    backgroundPosition:'50% 50%',
}
const divPart2 = {
    width:'100%',
    height:'70%',
    float:'left',
}
const divPart4 = {
    width:'100%',
    height:'60%',
    float:'left',
    background:'white',
    borderRadius:'15px',
}
const divPart5 = {
    width:'80px',
    height:'30%',
    float:'none',
    marginTop:'-30px',
    marginLeft:'auto',
    marginRight:'auto'
}
const divSpanBox1 = {
    width:'auto',
    height:'100%',
    float:'left',  
}
const spanStyle = {
    color: 'gray',
    fontSize: '0.8em'
};
const icon = {
    width:'18px',
    height:'18px',
    float:'left',
    marginRight:'5px',
    backgroundImage:"url(http://pfnew9fvk.bkt.clouddn.com/%E8%AD%A6%E5%91%8A.png)",
    backgroundSize:'100% 100%',
    backgroundRepeat:'no-repeat',
    backgroundPosition:'50% 50%' 
}
let icon1 = {...icon}
let icon2 = {...icon}
let icon3 = {...icon}
icon1.backgroundImage = "url(http://pfnew9fvk.bkt.clouddn.com/%E4%BA%A4%E6%98%93%E9%87%91%E9%A2%9D.png)"
icon2.backgroundImage = "url(http://pfnew9fvk.bkt.clouddn.com/%E4%BA%A4%E6%98%93%E5%8D%A1%E5%8F%B7.png)"
icon3.backgroundImage = "url(http://pfnew9fvk.bkt.clouddn.com/%E9%93%B6%E8%A1%8C%E9%A2%84%E7%95%99%E6%89%8B%E6%9C%BA%E5%8F%B7.png)"

const userIcon = {
    width:'80px',
    height:'80px',
    float:'left',
    background:'url(http://pfnew9fvk.bkt.clouddn.com/%E6%B5%8B%E8%AF%95%E5%A4%B4%E5%83%8F.jpg)',
    backgroundSize:'100% 100%',
    backgroundRepeat:'no-repeat',
    backgroundPosition:'50% 50%',
    borderRadius:'50%'
}
const divtxt = {
    width:'100%',
    height:'1.2em',
    float:'left',
    color:'white',
    background:'#3366ff',
    textAlign:'center',
    borderRadius:'15px',
    marginTop:'8px'
}
const divrow = {
    width:'100%',
    height:'33%',
    float:'left'
}
const divInputBox = {
    width:'90%',
    height:'54%',
    float:'left',
    padding:'0px',
    borderBottom:'1px solid #eeeeee'
}
const divInput = {
    width:'100%',
    height:'100%',
    textAlign:'right',
    border:'0px'
}
const divBtn = {
    width:'92%',
    height:'2.3em',
    color:'white',
    fontSize:'1.15em',
    background:'#2e76ff',
    textAlign:'center',
    borderRadius:'25px'
}