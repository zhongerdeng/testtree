import React, { Component } from 'react';
import {Link } from "react-router-dom";
import './mypay.css'
import 'weui'
import weui from 'weui.js'
import $ from 'jquery'
import queryData from '../action/request'
import DocumentTitle from 'react-document-title'
import { UserContext } from '../userContext';
import { Toast} from 'antd-mobile';

const isIPhone = new RegExp('\\biPhone\\b|\\biPod\\b', 'i').test(window.navigator.userAgent);
let wrapProps;
if (isIPhone) {
  wrapProps = {
    onTouchStart: e => e.preventDefault(),
  };
}
var actionIndex=1
class Renzheng extends Component{

    constructor(props) {
        super(props);
        this.state = {
            id:props.userInfo.userid,
            bgImg_1:"",
            bgImg_2:"",
            bgImg_3:"",
            imgZheng:"http://pfnew9fvk.bkt.clouddn.com/%E8%BA%AB%E4%BB%BD%E8%AF%81%E6%AD%A3%E9%9D%A2.png",
            imgFan:"http://pfnew9fvk.bkt.clouddn.com/%E8%BA%AB%E4%BB%BD%E8%AF%81%E5%8F%8D%E9%9D%A2.png",
            imgShouchi:"http://pfnew9fvk.bkt.clouddn.com/%E6%89%8B%E6%8C%81%E8%BA%AB%E4%BB%BD%E8%AF%81.png",
            zhengmian:(<span  className="v_center" style={spanStyle}>请拍摄<font style={{color:'#2e76ff'}}>正面</font>身份证</span>),
            fanmian:(<span  className="v_center" style={spanStyle}>请拍摄<font style={{color:'#2e76ff'}}>反面</font>身份证</span>),
            shouchi:(<span  className="v_center" style={spanStyle}>请拍摄<font style={{color:'#2e76ff'}}>手持</font>身份证</span>),
            
           
        };
      }

      componentDidMount(){
          //this.upImgAction();
      }
      upImgAction = () => {
        
        $('#uploaderInput').trigger('click')

        var uploadCount = 0;
        const that = this;
        weui.uploader('#uploader', {
           url: '/qiniu',
           auto: true,
           type: 'file',
           fileVal: 'fileVal',
           compress: {
               width: 1600,
               height: 1600,
               quality: .8
           },
           onBeforeQueued: function(files) {
               // `this` 是轮询到的文件, `files` 是所有文件
        
               if(["image/jpg", "image/jpeg", "image/png", "image/gif"].indexOf(this.type) < 0){
                   weui.alert('请上传图片');
                   return false; // 阻止文件添加
               }
               if(this.size > 10 * 1024 * 1024){
                   weui.alert('请上传不超过10M的图片');
                   return false;
               }
               if (files.length > 5) { // 防止一下子选择过多文件
                   weui.alert('最多只能上传5张图片，请重新选择');
                   return false;
               }
               if (uploadCount + 1 > 5) {
                   weui.alert('最多只能上传5张图片');
                   return false;
               }
        
               ++uploadCount;
        
               // return true; // 阻止默认行为，不插入预览图的框架
           },
           onQueued: function(){
               console.log(this);
        
               // console.log(this.status); // 文件的状态：'ready', 'progress', 'success', 'fail'
               // console.log(this.base64); // 如果是base64上传，file.base64可以获得文件的base64
        
               // this.upload(); // 如果是手动上传，这里可以通过调用upload来实现；也可以用它来实现重传。
               // this.stop(); // 中断上传
        
               // return true; // 阻止默认行为，不显示预览图的图像
           },
           onBeforeSend: function(data, headers){
               console.log(this, data, headers);
               // $.extend(data, { test: 1 }); // 可以扩展此对象来控制上传参数
               // $.extend(headers, { Origin: 'http://127.0.0.1' }); // 可以扩展此对象来控制上传头部
        
               // return false; // 阻止文件上传
           },
           onProgress: function(procent){
               console.log(this, procent);
               // return true; // 阻止默认行为，不使用默认的进度显示
           },
           onSuccess: function (ret) {
              // console.log("处理图片返回");
              // console.log(this, ret);
               if (ret.url){
                   switch(actionIndex){
                       case 1:
                        that.setState({
                            bgImg_1:ret.url,
                            imgZheng:"",
                            zhengmian:""
                        });break;
                        case 2:
                        that.setState({
                            bgImg_2:ret.url,
                            imgFan:"",
                            fanmian:""
                        });break;
                        case 3:
                        that.setState({
                            bgImg_3:ret.url,
                            imgShouchi:"",
                            shouchi:""
                        });break;
                   }
                 
               }
               // return true; // 阻止默认行为，不使用默认的成功态
           },
           onError: function(err){
               console.log(this, err);
               // return true; // 阻止默认行为，不使用默认的失败态
           }
        });
      }
      submitPersonInfo = () => {
          const params = {
              id:this.state.id,
              url1:this.state.bgImg_1,
              url2:this.state.bgImg_2,
              url3:this.state.bgImg_3,
          }
          queryData('/user/verifyPersonInfo?_xyz',params,(data)=>{
              console.log('data')
          })
          console.log('submitPersonInfo')
      }
    render(){
       
      

        return (
            <DocumentTitle title="认证中心">
            <div style={divbody}>
                 <div style={{width:'100%',height:'5%',float:'left',textAlign:'center'}}>
                    <div style={{width:'auto',height:'100%',textAlign:'center'}}>
                        <div className="v_center" style={icon1}/>
                        <span  className="v_center" style={{width:'auto',fontSize:'1em',display:'inline-block'}}>我们注重信息保护，不授权不对外提供</span>
                    </div>
                </div>
                <div style={{width:'100%',height:'95%',float:'left'}}>
                    <div className="h_center" style={divCard} onClick={()=>{
                        actionIndex=1
                        this.upImgAction();
                        }}
                    >
                    <div style={{width:'100%',height:'100%',float:'left',
                            backgroundImage:`url(${this.state.bgImg_1})`,
                            backgroundSize:'100% 100%',
                            backgroundPosition:'50% 50%',
                            backgroundRepeat:'no-repeat'
                        }}>
                            <div className='h_center' style={{width:160,height:'100%'}}>
                                <div style={{
                                    width:'100%',
                                    height:'80%',
                                    float:'left',
                                    backgroundImage:`url(${this.state.imgZheng})`,
                                    backgroundSize:'107px 107px',
                                    backgroundPosition:'50% 50%',
                                    backgroundRepeat:'no-repeat'

                                }}/>
                                <div style={{width:'100%',height:'20%',float:'left',textAlign:'center'}}>
                                    {this.state.zhengmian}
                                </div>
                            </div>
                        </div> 
                    </div>
                    <div className="h_center" style={divCard} onClick={()=>{
                        actionIndex=2
                        this.upImgAction();
                        }}
                    >
                    <div className="h_center" style={{width:'100%',height:'100%',float:'left',
                            backgroundImage:`url(${this.state.bgImg_2})`,
                            backgroundSize:'100% 100%',
                            backgroundPosition:'50% 50%',
                            backgroundRepeat:'no-repeat'
                        }}>
                            <div className='h_center' style={{width:160,height:'100%'}}>
                                <div style={{
                                    width:'100%',
                                    height:'80%',
                                    float:'left',
                                    backgroundImage:`url(${this.state.imgFan})`,
                                    backgroundSize:'107px 107px',
                                    backgroundPosition:'50% 50%',
                                    backgroundRepeat:'no-repeat'

                                }}/>
                                <div style={{width:'100%',height:'20%',float:'left',textAlign:'center'}}>
                                    {this.state.fanmian}
                                </div>
                            </div>
                        </div> 
                    </div>
                    <div className="h_center" style={divCard} onClick={()=>{
                        actionIndex=3
                        this.upImgAction();
                        }}
                    >
                    <div className="h_center" style={{width:'100%',height:'100%',float:'left',
                            backgroundImage:`url(${this.state.bgImg_3})`,
                            backgroundSize:'100% 100%',
                            backgroundPosition:'50% 50%',
                            backgroundRepeat:'no-repeat'
                        }}>
                            <div className='h_center' style={{width:160,height:'100%'}}>
                                <div style={{
                                    width:'100%',
                                    height:'80%',
                                    float:'left',
                                    backgroundImage:`url(${this.state.imgShouchi})`,
                                    backgroundSize:'107px 107px',
                                    backgroundPosition:'50% 50%',
                                    backgroundRepeat:'no-repeat'

                                }}/>
                                <div style={{width:'100%',height:'20%',float:'left',textAlign:'center'}}>
                                    {this.state.shouchi}
                                </div>
                            </div>
                        </div> 
                    </div>
                    <div style={{height:'20%',float:'left',width:'100%'}}>
                        <div style={{width:'100%',height:'2.7em',float:'left',textAlign:'center'}}>
                            <div style={{width:'auto',height:'100%',textAlign:'center'}}>
                                <div className="v_center" style={icon2}/>
                                <span  className="v_center" style={{width:'auto',fontSize:'1em',display:'inline-block'}}>身份证拍摄说明与实例</span>
                            </div>
                        </div>
                        <div style={{width:'100%',height:100,float:'left'}}>
                            <Link to={'/kaihu'} className="v_center">
                                <div className="center " style={divBtn} onClick={()=>{this.submitPersonInfo()}}>
                                    <span className="v_center" style={{color:'white',fontSize:'1em'}}>下 一 步</span>
                                </div>
                            </Link>
                        </div> 
                    </div>
                    <div id="uploader" style={{visibility:'hidden'}}>
                        <input id="uploaderInput" className="weui-uploader__input" type="file" accept="image/*" capture="camera" multiple="" />
                    </div>
                </div>
            </div>
            </DocumentTitle>
        )
    }
}
const RenzhengConsumer = ({ }) => (
    <UserContext.Consumer>
        {user => {
            return (
                <Renzheng
                    userInfo={user.userInfo}
                />
            )
        }}
    </UserContext.Consumer>
)
export default RenzhengConsumer

const divbody = {
    width:'100%',
    height:'100%',
    background:'#f1f1f1'
}
const spanStyle = {
    color: 'gray',
    fontSize: '0.9em'
};
const divCard = {
    width:'94%',
    height:'24%',
    marginBottom:'3%',
    background:'white',
};
const icon1 = {
    width:'16px',
    height:'16px',
    display:'inline-block',
    backgroundImage:'url(http://pfnew9fvk.bkt.clouddn.com/%E5%AE%89%E5%85%A8%E8%A7%84%E5%88%99%E7%94%A8.png)',
    backgroundSize:'100% 100%',
    backgroundRepeat:'no-repeat',
    backgroundPosition:'50% 50%' ,
    marginRight:10
}
const icon2 = {...icon1}
icon2.backgroundImage = 'url(http://pfnew9fvk.bkt.clouddn.com/%E8%AD%A6%E5%91%8A.png)'

const divBtn = {
    width:'92%',
    height:'2.3em',
    color:'white',
    fontSize:'1.15em',
    background:'#2e76ff',
    textAlign:'center',
    borderRadius:'25px'
}