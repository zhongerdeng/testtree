import React, { Component } from 'react';
import ReactDOM from 'react-dom'
import './mypay.css'
import queryData from '../action/request'
import { Toast} from 'antd-mobile';
import { UserContext } from '../userContext';
import PropTypes from 'prop-types'

class Kaihu extends Component{

    constructor(props){
        super(props)
        this.state={
            id:props.userInfo.userid,
            bodyHeight:document.documentElement.clientHeight,
            imgHeight:document.documentElement.clientHeight,
            formHeight:document.documentElement.clientHeight,
            cardNumber:'',
            name:''
        }
    }
    componentDidMount(){
       
        const bodyHeight = `${document.documentElement.clientHeight}px`
        const imgHei = `${document.documentElement.clientHeight*0.3}px`
        const formHei = `${document.documentElement.clientHeight*0.3}px`
        this.setState({
            bodyHeight:bodyHeight,
            imgHeight:imgHei,
            formHeight:formHei
        })
    }
    submitInfo = () => {
        const cardName = ReactDOM.findDOMNode(this.refs.cardName).value
        const cardId = ReactDOM.findDOMNode(this.refs.cardId).value
        const cardNum = ReactDOM.findDOMNode(this.refs.cardNum).value
        const cardPhone = ReactDOM.findDOMNode(this.refs.cardPhone).value
        const params ={
            id:this.state.id,
            cardId:cardId,
            bankcardNum:cardNum,
            name:cardName,
            reservedPhone:cardPhone
        }
    
       queryData('/account/addAccount?_xyz',params).then((data)=>{
            console.log(data)
            if(data.status== true ){
                this.context.router.history.push("./waitshenhe");
            }else if(data.status == false){
                Toast.fail(data.message,1.5)
            }
            
       })
    }
    render(){
        
        const text = () => (
            <div style={{fontSize:'0.8em',color:'#e60012'}}>
                <div><span>结算卡是接收交易结算资金的借记卡，为保证提现资金实时到帐，请务必认真填写并仔细核对！</span></div>
                <div><span>持卡人姓名和身份证号必须与上传的身份证照片信息一致</span></div>
            </div>
        )
        return (
            <div style={{
                width:'100%',
                height:`${this.state.bodyHeight}`,
                float:'left',
                background:'white'
            }}>
                <div style={divbody}>
                    <div style={emptyStyle}/>
                    <div style={{
                        height:`${this.state.imgHeight}`,
                        background:'url(http://pfnew9fvk.bkt.clouddn.com/%E7%BB%93%E7%AE%97%E5%8D%A1_2.jpg)',
                        backgroundSize:'100% 100%',
                        position:'relative',
                    }}>
                        <div style={{
                            width:'auto',
                            height:'auto',
                            position:'absolute',
                            left:'7.5%',
                            top:'5.5%',
                        }}>
                            <span style={{color:'white',fontSize:'1.5em'}}>结算卡</span>
                        </div>
                        <div style={{
                            width:'auto',
                            height:'auto',
                            position:'absolute',
                            left:'7.5%',
                            top:'55%',
                        }}>
                            <span style={{color:'white',fontSize:'1.4em'}}>{this.state.cardNumber}</span>
                        </div>
                        <div style={{
                            width:'auto',
                            height:'auto',
                            position:'absolute',
                            left:'77%',
                            top:'80%',
                        }}>
                            <span style={{color:'white',fontSize:'1.2em'}}>{this.state.name}</span>
                        </div>
                    </div>
                    <div style={{
                        height:`${this.state.formHeight}`,
                        marginTop:'2%',
                        background:'white'
                    }}>
                        <div style={divInput}>
                            <input ref="cardName" style={input} placeholder='&#8194;持 卡 人 姓 名' onChange={()=>{
                                let tm = ReactDOM.findDOMNode(this.refs.cardName).value
                                this.setState({
                                    name:tm
                                })
                            }}/>
                        </div>
                        <div style={divInput}>
                            <input ref="cardId"  style={input} placeholder='&#8194;身 份 证 号 码'/>
                        </div>
                        <div style={divInput}>
                            <input ref="cardNum"  style={input} placeholder='&#8194;银 行 卡 账 号' onChange={()=>{
                                let tm = ReactDOM.findDOMNode(this.refs.cardNum).value
                                this.setState({
                                    cardNumber:tm
                                })
                            }}/>
                        </div>
                        <div style={divInput}>
                            <input ref="cardPhone"  style={input} placeholder='&#8194;银 行 预 留 手 机 号' />
                        </div>
                    </div>
                    <div style={{width:'100%',height:'32%',float:'left'}}>
                        <div style={txtStyle}>
                           {text()}
                        </div>
                        <div style={{width:'100%',height:'50%',float:'left',marginTop:'25px'}}>
                            <div className="h_center v_center" style={divBtn} onClick={this.submitInfo}>
                                <span className="v_center" style={{color:'white',fontSize:'1em'}}>添 加 结 算 卡</span>
                            </div>
                        </div>   
                    </div>
                </div>
            </div>
        )
    }
}
const KaihuConsumer = ({ }) => (
    <UserContext.Consumer>
        {user => {
            return (
                <Kaihu
                    userInfo={user.userInfo}
                />
            )
        }}
    </UserContext.Consumer>
)
export default KaihuConsumer
Kaihu.contextTypes = {
    router: PropTypes.object.isRequired
}
const divbody = {
    width:'90%',
    height:'100%',
    float:'none',
    margin:'auto'
}
const emptyStyle = {
    height:'2%',
}
const divInput = {
    height:'22%',
    borderBottom:'1px solid #eeeeee',
    marginTop:'2%'
}
const txtStyle = {
   // height:'50%',
    float:'left',
    marginTop:'15px'
}
const input = {
    width:'100%',
    height:'100%',
    padding:'0px',
    border:'0px',
    fontSize:'0.9em',
    color:'black'
}
const divBtn = {
    width:'92%',
    height:'2.3em',
    color:'white',
    fontSize:'1.15em',
    background:'#2e76ff',
    textAlign:'center',
    borderRadius:'25px'
}