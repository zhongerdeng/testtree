import React, { Component } from 'react';
import './mypay.css'

export default class Sucess extends Component{
    render(){
        const divbody = {
            width:'100%',
            height:'100%',
            float:'left',
            background:'#f1f1f1'
        }
        const divStyle1 = {
            width:'100%',
            height:'18%',
            float:'left'
        }
        const divimg = {
            width:'70px',
            height:'70px',
            float:'none',
            margin:'auto',
            background:'url(http://pe3kd7kwd.bkt.clouddn.com/%E6%94%AF%E4%BB%98%E5%A4%B1%E8%B4%A5.png)',
            backgroundSize:'100% 100%',
            backgroundRepeat:'no-repeat',
            backgroundPosition:'0% 0%',
        }
        const divStyle2 = {
            width:'100%',
            height:'auto',
            float:'left',
            textAlign:'center',
            fontSize:'2em',
            color:'#333'
        }
        const divStyle3 = {
            width:'100%',
            height:'auto',
            float:'left',
            marginTop:'10px',
            textAlign:'center',
            fontSize:'1em',
            color:'gray'
        }
        const divStyle4 = {
            width:'100%',
            height:'20%',
            float:'left',
            marginTop:'30px',
        }
        const divBtn = {
            width:'94%',
            height:'3em',
            float:'none',
            margin:'auto',
            textAlign:'center',
            background:'green',
            color:'white',
            borderRadius:'8px',
            fontSize:'1.2em  '
        }
        return(
            <div style={divbody}>
                <div style={divStyle1}>
                    <div  className="v_center" style={divimg}/>
                </div>
                <div style={divStyle2}>支付失败</div>
                <div style={divStyle3}>失败原因：银行系统异常，请联系客服</div>
                <div style={divStyle4}>

                    <div style={divBtn}><span className="v_center">重新支付</span></div>
                </div>
                
            </div>   
        )
    }
}