const path = require('path');

const alias = {
    client: path.join(__dirname, '../client'),
    images: path.join(__dirname, '../client/images'),
    themes: path.join(__dirname, '../client/themes'),
};

module.exports = {
    keys: 'kjpay',
    webpack: {
        custom: {
          configPath: path.join(__dirname, './webpack.js'),
        },
    },
    react: {
        static: true,
        assetHost: '//pfnew9fvk.bkt.clouddn.com',
        assetPath: '/kjpay/0.1.1',
    },
    isomorphic: {
        alias,
    },
    security : {
        csrf: {
          enable:false,
        },
    }
};