const path = require('path');
const alias = {
    client: path.join(__dirname, '../client'),
    images: path.join(__dirname, '../client/images'),
    themes: path.join(__dirname, '../client/themes'),
};

module.exports = {
    keys: 'lmmh5frame',
    react: {
        static: true,
    },
    webpack: {
        custom: {
          configPath: path.join(__dirname, './webpack.js'),
        },
    },
    isomorphic: {
        alias,
    },
    security : {
        csrf: {
          enable:false,
        },
    }
};

  